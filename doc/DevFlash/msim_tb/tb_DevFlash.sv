`timescale 1ns / 100ps

module tb_DevFlash();

intf_sys	      sys_hrd();
// --
intf_flash   	 flash();

DevFlash DevFlash_inst(sys_hrd, flash);
test test_inst(sys_hrd, flash);

endmodule
