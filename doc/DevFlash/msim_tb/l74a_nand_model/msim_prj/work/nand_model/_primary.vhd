library verilog;
use verilog.vl_types.all;
entity nand_model is
    generic(
        MAX_ASYNC_TIM_MODE: integer := 5;
        tBERS_min       : integer := 3800000;
        tBERS_max       : integer := 10000000;
        tCBSY_min       : integer := 35000;
        tCBSY_max       : integer := 2600000;
        tDBSY_min       : integer := 500;
        tDBSY_max       : integer := 1000;
        tFEAT           : integer := 1000;
        tITC_max        : integer := 1000;
        tLBSY_min       : integer := 2000;
        tLBSY_max       : integer := 3000;
        tOBSY_max       : integer := 40000;
        tPROG_typ       : integer := 1300000;
        tPROG_max       : integer := 2600000;
        NPP             : integer := 1;
        tRCBSY_max      : integer := 3000;
        tDCBSYR1_max    : vl_notype;
        tR_max          : integer := 75000;
        tR_mp_max       : integer := 75000;
        tRST_read       : integer := 5000;
        tRST_prog       : integer := 10000;
        tRST_erase      : integer := 500000;
        tRST_powerup    : integer := 1000000;
        tRST_ready      : integer := 5000;
        tVCC_delay      : integer := 10000;
        tRB_PU_max      : integer := 100000;
        tCLHIO_min      : integer := 0;
        tCLSIO_min      : integer := 0;
        tDHIO_min       : integer := 0;
        tDSIO_min       : integer := 0;
        tREAIO_max      : integer := 0;
        tRPIO_min       : integer := 0;
        tWCIO_min       : integer := 0;
        tWHIO_min       : integer := 0;
        tWHRIO_min      : integer := 0;
        tWPIO_min       : integer := 0;
        tEDO_RC         : integer := 30;
        MAX_SYNC_TIM_MODE: integer := 5;
        PAGE_BITS       : integer := 8;
        COL_BITS        : integer := 14;
        COL_CNT_BITS    : integer := 14;
        DQ_BITS         : integer := 8;
        NUM_OTP_ROW     : integer := 30;
        OTP_ADDR_MAX    : vl_notype;
        OTP_NPP         : integer := 4;
        NUM_BOOT_BLOCKS : integer := 0;
        BOOT_BLOCK_BITS : integer := 1;
        LUN_BITS        : integer := 0;
        ROW_BITS        : integer := 20;
        BLCK_BITS       : integer := 12;
        NUM_ROW         : integer := 1024;
        NUM_COL         : integer := 8640;
        NUM_PAGE        : vl_notype;
        NUM_PLANES      : integer := 2;
        NUM_BLCK        : vl_notype;
        BPC_MAX         : vl_logic_vector(0 to 2) := (Hi0, Hi0, Hi1);
        BPC             : vl_logic_vector(0 to 2) := (Hi0, Hi0, Hi1);
        PAGE_SIZE       : vl_notype;
        NUM_ID_BYTES    : integer := 8;
        READ_ID_BYTE0   : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi1, Hi0, Hi1, Hi1, Hi0, Hi0);
        READ_ID_BYTE1   : vl_logic_vector(0 to 7) := (Hi1, Hi0, Hi0, Hi0, Hi1, Hi0, Hi0, Hi0);
        READ_ID_BYTE2   : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi1, Hi0, Hi0);
        READ_ID_BYTE3   : vl_logic_vector(0 to 7) := (Hi0, Hi1, Hi0, Hi0, Hi1, Hi0, Hi1, Hi1);
        READ_ID_BYTE4   : vl_logic_vector(0 to 7) := (Hi1, Hi0, Hi1, Hi0, Hi1, Hi0, Hi0, Hi1);
        READ_ID_BYTE5   : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0);
        READ_ID_BYTE6   : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0);
        READ_ID_BYTE7   : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0);
        FEATURE_SET     : vl_logic_vector(0 to 15) := (Hi1, Hi1, Hi0, Hi0, Hi0, Hi1, Hi1, Hi0, Hi0, Hi1, Hi0, Hi0, Hi1, Hi0, Hi1, Hi1);
        FEATURE_SET2    : vl_logic_vector(0 to 15) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1, Hi0, Hi1, Hi0);
        DRIVESTR_EN     : vl_logic_vector(0 to 2) := (Hi0, Hi1, Hi1);
        NOONFIRDCACHERANDEN: vl_logic_vector(0 to 2) := (Hi0, Hi0, Hi0);
        NUM_DIE         : integer := 1;
        NUM_CE          : integer := 1;
        async_only_n    : vl_logic := Hi0;
        MAX_LUN_PER_TAR : integer := 2
    );
    port(
        Dq_Io           : inout  vl_logic_vector;
        Cle             : in     vl_logic;
        Ale             : in     vl_logic;
        Clk_We_n        : in     vl_logic;
        Wr_Re_n         : in     vl_logic;
        Dqs             : inout  vl_logic;
        Ce_n            : in     vl_logic;
        Wp_n            : in     vl_logic;
        Rb_n            : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of MAX_ASYNC_TIM_MODE : constant is 1;
    attribute mti_svvh_generic_type of tBERS_min : constant is 1;
    attribute mti_svvh_generic_type of tBERS_max : constant is 1;
    attribute mti_svvh_generic_type of tCBSY_min : constant is 1;
    attribute mti_svvh_generic_type of tCBSY_max : constant is 1;
    attribute mti_svvh_generic_type of tDBSY_min : constant is 1;
    attribute mti_svvh_generic_type of tDBSY_max : constant is 1;
    attribute mti_svvh_generic_type of tFEAT : constant is 1;
    attribute mti_svvh_generic_type of tITC_max : constant is 1;
    attribute mti_svvh_generic_type of tLBSY_min : constant is 1;
    attribute mti_svvh_generic_type of tLBSY_max : constant is 1;
    attribute mti_svvh_generic_type of tOBSY_max : constant is 1;
    attribute mti_svvh_generic_type of tPROG_typ : constant is 1;
    attribute mti_svvh_generic_type of tPROG_max : constant is 1;
    attribute mti_svvh_generic_type of NPP : constant is 1;
    attribute mti_svvh_generic_type of tRCBSY_max : constant is 1;
    attribute mti_svvh_generic_type of tDCBSYR1_max : constant is 3;
    attribute mti_svvh_generic_type of tR_max : constant is 1;
    attribute mti_svvh_generic_type of tR_mp_max : constant is 1;
    attribute mti_svvh_generic_type of tRST_read : constant is 1;
    attribute mti_svvh_generic_type of tRST_prog : constant is 1;
    attribute mti_svvh_generic_type of tRST_erase : constant is 1;
    attribute mti_svvh_generic_type of tRST_powerup : constant is 1;
    attribute mti_svvh_generic_type of tRST_ready : constant is 1;
    attribute mti_svvh_generic_type of tVCC_delay : constant is 1;
    attribute mti_svvh_generic_type of tRB_PU_max : constant is 1;
    attribute mti_svvh_generic_type of tCLHIO_min : constant is 1;
    attribute mti_svvh_generic_type of tCLSIO_min : constant is 1;
    attribute mti_svvh_generic_type of tDHIO_min : constant is 1;
    attribute mti_svvh_generic_type of tDSIO_min : constant is 1;
    attribute mti_svvh_generic_type of tREAIO_max : constant is 1;
    attribute mti_svvh_generic_type of tRPIO_min : constant is 1;
    attribute mti_svvh_generic_type of tWCIO_min : constant is 1;
    attribute mti_svvh_generic_type of tWHIO_min : constant is 1;
    attribute mti_svvh_generic_type of tWHRIO_min : constant is 1;
    attribute mti_svvh_generic_type of tWPIO_min : constant is 1;
    attribute mti_svvh_generic_type of tEDO_RC : constant is 1;
    attribute mti_svvh_generic_type of MAX_SYNC_TIM_MODE : constant is 1;
    attribute mti_svvh_generic_type of PAGE_BITS : constant is 1;
    attribute mti_svvh_generic_type of COL_BITS : constant is 1;
    attribute mti_svvh_generic_type of COL_CNT_BITS : constant is 1;
    attribute mti_svvh_generic_type of DQ_BITS : constant is 1;
    attribute mti_svvh_generic_type of NUM_OTP_ROW : constant is 1;
    attribute mti_svvh_generic_type of OTP_ADDR_MAX : constant is 3;
    attribute mti_svvh_generic_type of OTP_NPP : constant is 1;
    attribute mti_svvh_generic_type of NUM_BOOT_BLOCKS : constant is 1;
    attribute mti_svvh_generic_type of BOOT_BLOCK_BITS : constant is 1;
    attribute mti_svvh_generic_type of LUN_BITS : constant is 1;
    attribute mti_svvh_generic_type of ROW_BITS : constant is 1;
    attribute mti_svvh_generic_type of BLCK_BITS : constant is 1;
    attribute mti_svvh_generic_type of NUM_ROW : constant is 1;
    attribute mti_svvh_generic_type of NUM_COL : constant is 1;
    attribute mti_svvh_generic_type of NUM_PAGE : constant is 3;
    attribute mti_svvh_generic_type of NUM_PLANES : constant is 1;
    attribute mti_svvh_generic_type of NUM_BLCK : constant is 3;
    attribute mti_svvh_generic_type of BPC_MAX : constant is 1;
    attribute mti_svvh_generic_type of BPC : constant is 1;
    attribute mti_svvh_generic_type of PAGE_SIZE : constant is 3;
    attribute mti_svvh_generic_type of NUM_ID_BYTES : constant is 1;
    attribute mti_svvh_generic_type of READ_ID_BYTE0 : constant is 1;
    attribute mti_svvh_generic_type of READ_ID_BYTE1 : constant is 1;
    attribute mti_svvh_generic_type of READ_ID_BYTE2 : constant is 1;
    attribute mti_svvh_generic_type of READ_ID_BYTE3 : constant is 1;
    attribute mti_svvh_generic_type of READ_ID_BYTE4 : constant is 1;
    attribute mti_svvh_generic_type of READ_ID_BYTE5 : constant is 1;
    attribute mti_svvh_generic_type of READ_ID_BYTE6 : constant is 1;
    attribute mti_svvh_generic_type of READ_ID_BYTE7 : constant is 1;
    attribute mti_svvh_generic_type of FEATURE_SET : constant is 1;
    attribute mti_svvh_generic_type of FEATURE_SET2 : constant is 1;
    attribute mti_svvh_generic_type of DRIVESTR_EN : constant is 1;
    attribute mti_svvh_generic_type of NOONFIRDCACHERANDEN : constant is 1;
    attribute mti_svvh_generic_type of NUM_DIE : constant is 1;
    attribute mti_svvh_generic_type of NUM_CE : constant is 1;
    attribute mti_svvh_generic_type of async_only_n : constant is 1;
    attribute mti_svvh_generic_type of MAX_LUN_PER_TAR : constant is 1;
end nand_model;
