# -- SYS
#set_instance_assignment -name VIRTUAL_PIN ON -to sys.clk_5
#set_instance_assignment -name VIRTUAL_PIN ON -to sys.reset
set_location_assignment PIN_B5 -to sys_hrd.reset_n
# -- ATT
set_location_assignment PIN_E10 -to att.out[20]
set_location_assignment PIN_E11 -to att.out[19]
set_location_assignment PIN_F10 -to att.out[18]
set_location_assignment PIN_F11 -to att.out[17]
set_location_assignment PIN_G10 -to att.out[16]
set_location_assignment PIN_G11 -to att.out[15]
set_location_assignment PIN_H10 -to att.out[14]
set_location_assignment PIN_H11 -to att.out[13]
set_location_assignment PIN_J11 -to att.out[12]
set_location_assignment PIN_J10 -to att.out[11]
set_location_assignment PIN_K11 -to att.out[10]
set_location_assignment PIN_K10 -to att.out[9]
set_location_assignment PIN_L11 -to att.out[8]
set_location_assignment PIN_L10 -to att.out[7]
set_location_assignment PIN_K9 -to att.out[6]
set_location_assignment PIN_L9 -to att.out[5]
set_location_assignment PIN_K8 -to att.out[4]
set_location_assignment PIN_L8 -to att.out[3]
set_location_assignment PIN_K7 -to att.out[2]
set_location_assignment PIN_L7 -to att.out[1]
set_location_assignment PIN_K6 -to att.out[0]
# -- RS232
set_location_assignment PIN_D11 -to rs232.rx
set_location_assignment PIN_C10 -to rs232.tx
# -- HOST
set_location_assignment PIN_B6 -to hst.DB[0]
set_location_assignment PIN_A6 -to hst.DB[1]
set_location_assignment PIN_A5 -to hst.DB[2]
set_location_assignment PIN_B4 -to hst.DB[3]
set_location_assignment PIN_A3 -to hst.DB[4]
set_location_assignment PIN_B3 -to hst.DB[5]
set_location_assignment PIN_B2 -to hst.DB[6]
set_location_assignment PIN_A2 -to hst.DB[7]
set_location_assignment PIN_A1 -to hst.CEn
set_location_assignment PIN_B1 -to hst.WE_RDn
set_location_assignment PIN_C1 -to hst.STRB
set_location_assignment PIN_C2 -to hst.INTn[0]
set_location_assignment PIN_D2 -to hst.INTn[1]
set_location_assignment PIN_D1 -to hst.INTn[2]
set_location_assignment PIN_E1 -to hst.INTn[3]
set_location_assignment PIN_F2 -to hst.INTn[4]
set_location_assignment PIN_F1 -to hst.INTn[5]
set_location_assignment PIN_G2 -to hst.INTn[6]
set_location_assignment PIN_G1 -to hst.INTn[7]
set_location_assignment PIN_H2 -to hst.INTn[8]
set_location_assignment PIN_H1 -to hst.INTn[9]
# -- CPR
set_location_assignment PIN_B11 -to gps_cpr.rst
set_location_assignment PIN_B8 -to gps_cpr.pps
set_location_assignment PIN_A7 -to gps_cpr.rx[0]
set_location_assignment PIN_A8 -to gps_cpr.rx[1]
set_location_assignment PIN_D10 -to gps_cpr.tx[0]
set_location_assignment PIN_B7 -to gps_cpr.tx[1]
# -- MNP
set_location_assignment PIN_A9 -to gps_mnp.rst_n
set_location_assignment PIN_B9 -to gps_mnp.pps
set_location_assignment PIN_A11 -to gps_mnp.rx
set_location_assignment PIN_A10 -to gps_mnp.tx
# -- DDS
#set_instance_assignment -name VIRTUAL_PIN ON -to dds.data
#set_instance_assignment -name VIRTUAL_PIN ON -to dds.wr_data
set_location_assignment PIN_K4 -to dds_hrd.rst
set_location_assignment PIN_L6 -to dds_hrd.D7
set_location_assignment PIN_K5 -to dds_hrd.clk
set_location_assignment PIN_L5 -to dds_hrd.fq
set_location_assignment PIN_L4 -to dds_hrd.en_100MHz