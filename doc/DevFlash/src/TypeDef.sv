`ifndef TypeDefSV
`define TypeDefSV

//-----------------------------------------------------------------------------
// System inrefaces
//-----------------------------------------------------------------------------
interface intf_sys;
	wire clk_50, reset_n;
	
	modport hrd(
		input clk_50, reset_n
	);
	modport tb(
		output clk_50, reset_n
	);
endinterface
interface intf_sys_sw;
	wire clk_50, clk_10, reset, reset_n;
	
	modport sys(
		output clk_50, clk_10, reset, reset_n
	);
	modport sw(
		input clk_50, clk_10, reset, reset_n
	);
endinterface
//-----------------------------------------------------------------------------
// flash hard
//-----------------------------------------------------------------------------
interface intf_flash;
    wire [7:0] DQ;
    wire [3:0] CE;
    wire WEn, RDEn, ALE, CLE, WP, RB;
	
	modport hrd(
		inout DQ, 
		output WEn, RDEn, ALE, CLE, WP,
		input RB
	);
	modport tb(
		inout DQ, 
		input WEn, RDEn, ALE, CLE, WP,
		output RB
	);
endinterface
interface intf_flash_sw;
    wire [7:0] DQ;
    wire [1:0] CE, RB;
    wire WEn, REn, ALE, CLE, WP, RB;
	
	modport sw(
		inout DQ, 
		output WEn, RDEn, ALE, CLE, WP,
		input RB
	);
endinterface


`endif