`timescale 1 ps / 1 ps

`include "../src/defined.sv"

module host_module(
  intf_sys_sw.sw   	sys,
  intf_host.host    hst,
  // --
  intf_att.host     att,
  intf_rs232.host   rs232,
  intf_gps_cpr.host gps_cpr,
  intf_gps_mnp.host	gps_mnp,
  intf_dds.hrd  	   dds_hrd,
  intf_dds_sw.host	 dds
);

typedef enum {MdIDLE, MdRdAddr, MdSetData, MdGetData} enMode;
logic [1:0] snc_CEn, snc_STRB, MdRdData;
logic [7:0] DB_out;
logic sel_cpr_pps, sel_mnp_pps;

`define CEn_FAIL 2'b10
`define CEn_LO   2'b00
`define STR_RIS  2'b01
always_ff @(posedge sys.clk_5) begin
    // -- CE
    snc_CEn <= (snc_CEn << 1) | hst.CEn;
    // -- STRB
    snc_STRB <= (snc_STRB << 1) | hst.STRB;
end

assign hst.DB = (!hst.WE_RDn) ? DB_out : 8'hZZ;

// -- INTn
//always_comb begin
    // -- RS232
assign    rs232.tx = hst.INTn[`INT_bit_RS232_TX];
assign    hst.INTn[`INT_bit_RS232_RX] = rs232.rx;
    // -- GSP
assign    hst.INTn[`INT_bit_PPS] = (gps_cpr.pps & sel_cpr_pps) | (gps_mnp.pps & sel_mnp_pps);
    // -- GPS_CPR
    genvar i;
    for(i = 0; i < 2; i++) begin : blck_CPR
        assign hst.INTn[`INT_bit_CPR_RX0 + i] = gps_cpr.rx[i];
        assign gps_cpr.tx[i] = hst.INTn[`INT_bit_CPR_TX0 + i];
    end
    // -- GPS_MNP
    assign hst.INTn[`INT_bit_MNP_RX0] = gps_mnp.rx;
    assign gps_mnp.tx = hst.INTn[`INT_bit_MNP_TX0];
//end
// - DDS
always_comb begin
    dds_hrd.rst = dds.rst;
    dds_hrd.D7 = dds.D7;
    dds_hrd.clk = dds.clk;
    dds_hrd.fq = dds.fq;
    dds_hrd.en_100MHz = dds.en_100MHz;
end


always_ff @(posedge sys.clk_5) begin : blck_statet_mach
    var enMode mode;
    var logic[7:0] ADDR;
    var logic [3:0] cnt_byte;
    var logic [2:0][7 : 0] att_out;
    
	if(sys.reset) begin
		cnt_byte = 4'h0;
		mode = MdIDLE;
		`set(DB_out, 1'b0);
		// -- ATT
		`set(att.out, 1'b0);
		`set(att_out, 1'b0);
		// -- DDS
		dds.rst = 1'b0;
        dds.en_100MHz = 1'b0;
        `set(dds.data, 1'b0);
        dds.wr_data = 1'b0;
        // -- GPS_CPR
        sel_cpr_pps = 1'b1;
        gps_cpr.rst = 1'b0;
        // -- GPS_MNP
        sel_mnp_pps = 1'b0;
	end else begin
        if((snc_CEn == `CEn_FAIL) || (snc_CEn == `CEn_LO)) case(mode)
            MdIDLE: begin
                 mode = MdRdAddr;
                 cnt_byte = 4'h0;
            end
            MdRdAddr: begin
                if(snc_STRB == `STR_RIS) begin
                    ADDR = {1'b0, hst.DB[6:0]};
                    if(hst.DB[7]) mode = MdSetData;
                    else mode = MdGetData;
                end
            end
            // -- SET DATA
            MdSetData: begin
                if(snc_STRB == `STR_RIS) begin
                    case(ADDR)
                    `ADDR_ATT_0: begin
                        if(cnt_byte < 4'h3) att_out[cnt_byte] = hst.DB;
                        else att.out = att_out;
                    end
                    `ADDR_DDS_CNTRL: begin
                        dds.rst = hst.DB[`DDS_bit_RST];
                        dds.en_100MHz = hst.DB[`DDS_bit_en100MHZ];
                    end
                    `ADDR_DDS_0: begin
                        if(cnt_byte < 4'h5) dds.data[cnt_byte] = hst.DB;
                        else if(cnt_byte < 4'h6) dds.wr_data = 1'b1;
                        else dds.wr_data = 1'b0;
                    end
                    `ADDR_CPR_CNTRL: begin
                        gps_cpr.rst = hst.DB[`CPR_bit_RST];
                        sel_cpr_pps = hst.DB[`CPR_bit_PPS];
                    end
                    `ADDR_MNP_CNTRL: begin
                        gps_mnp.rst_n = hst.DB[`CPR_bit_RST];
                        sel_mnp_pps = hst.DB[`CPR_bit_PPS];
                    end
                    endcase
                    cnt_byte++;
                end
            end
            // -- GET DATA
            MdGetData: begin
                if(snc_STRB == `STR_RIS) begin
                    case(ADDR)
                    `ADDR_ATT_0: begin
                        DB_out = att_out[cnt_byte];
                    end
                    `ADDR_DDS_CNTRL: begin
                        DB_out[`DDS_bit_RST] = dds.rst;
                        DB_out[`DDS_bit_en100MHZ] = dds.en_100MHz;
                    end
                    `ADDR_CPR_CNTRL: begin
                        DB_out[`CPR_bit_RST] = gps_cpr.rst;
                        DB_out[`CPR_bit_PPS] = sel_cpr_pps;
                    end
                    `ADDR_MNP_CNTRL: begin
                        DB_out[`MNP_bit_RST] = gps_mnp.rst_n;
                        DB_out[`MNP_bit_PPS] = sel_mnp_pps;
                    end
                    default:
                        DB_out = 8'hAA;
                    endcase
                    cnt_byte++;
                end
            end
            default:
                mode = MdIDLE;
            endcase
        else begin
            mode = MdIDLE;
        end
	end
end


endmodule

