`include "../src/defined.sv"

//-----------------------------------------------------------------------------
// System module
//-----------------------------------------------------------------------------
module system(
	intf_sys.hrd    sys,
	intf_sys_sw.sys sys_sw
);

logic [1:0] div;

assign sys_sw.reset_n = sys.reset_n;
assign sys_sw.reset = ~sys.reset_n;
assign sys_sw.clk_50 = sys.clk_50;

assign sys_sw.clk_10 = div[1];

always_ff @(posedge sys.clk_50) begin
    if(sys_sw.reset) begin
        div = {{($size(div) - 2){1'b0}}, 1'b1};
    end else begin
        div = (div << 1) | div[$size(div) - 1];
    end
end

endmodule
