library verilog;
use verilog.vl_types.all;
entity nand_interface is
    port(
        h_rd_wr         : in     vl_logic;
        h_cntrl         : in     vl_logic_vector(2 downto 0);
        e_d             : in     vl_logic;
        reset           : in     vl_logic;
        ce              : out    vl_logic;
        re              : out    vl_logic;
        ale             : out    vl_logic;
        cle             : out    vl_logic;
        se              : out    vl_logic;
        we              : out    vl_logic;
        wp              : out    vl_logic
    );
end nand_interface;
