--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   07:39:32 06/26/2013
-- Design Name:   
-- Module Name:   E:/work/CKB_RIAP/nand_flash_memory/fpga_project/xilinx/test_fifo1_2048_top.vhd
-- Project Name:  nand
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: fifo1_2048_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
    use IEEE.STD_LOGIC_ARITH.ALL; 
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_fifo1_2048_top IS
END test_fifo1_2048_top;
 
ARCHITECTURE behavior OF test_fifo1_2048_top IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT fifo1_2048_top
    PORT(
         iCLK_50MHZ : IN  std_logic;
         iRST : IN  std_logic;
         iDATA_DEV : IN  std_logic_vector(7 downto 0);
         iSTART_PACK_DEV : IN  std_logic;
         iRD_DATA_FAFO1 : IN  std_logic;
         oEMPTY_FIFO1 : OUT  std_logic;
         oFULL_FIFO1 : OUT  std_logic;
         oVALID_FIFO1 : out std_logic;
         oDATA_FIFO1 : OUT  std_logic_vector(7 downto 0);
         oCOUNT_PACK_FIFO1 : OUT  std_logic_vector(10 downto 0);
         oPACK_RCV_FIFO1 : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal iCLK_50MHZ      : std_logic := '0';
   signal iRST            : std_logic := '0';
   signal iDATA_DEV       : std_logic_vector(7 downto 0) := (others => '0');
   signal iSTART_PACK_DEV : std_logic := '0';
   signal iRD_DATA_FAFO1  : std_logic := '0';

 	--Outputs
   signal oEMPTY_FIFO1 : std_logic;
   signal oFULL_FIFO1 : std_logic;
   signal oVALID_FIFO1 : std_logic;
   signal oDATA_FIFO1 : std_logic_vector(7 downto 0);
   signal oCOUNT_PACK_FIFO1 : std_logic_vector(10 downto 0);
   signal oPACK_RCV_FIFO1 : std_logic;

   signal data_in       : std_logic_vector(7 downto 0) := "00000000";
   signal start_pckg    : std_logic := '0';
   
   
   constant iCLK_50MHZ_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: fifo1_2048_top PORT MAP (
          iCLK_50MHZ => iCLK_50MHZ,
          iRST => iRST,
          iDATA_DEV => iDATA_DEV,
          iSTART_PACK_DEV => iSTART_PACK_DEV,
          iRD_DATA_FAFO1 => iRD_DATA_FAFO1,
          oEMPTY_FIFO1 => oEMPTY_FIFO1,
          oFULL_FIFO1 => oFULL_FIFO1,
          oVALID_FIFO1 => oVALID_FIFO1,
          oDATA_FIFO1 => oDATA_FIFO1,
          oCOUNT_PACK_FIFO1 => oCOUNT_PACK_FIFO1,
          oPACK_RCV_FIFO1 => oPACK_RCV_FIFO1
        );

    -- Clock process definitions
    iCLK_50MHZ_process :process
    begin
	    iCLK_50MHZ <= '0';
		wait for iCLK_50MHZ_period/2;
		iCLK_50MHZ <= '1';
		wait for iCLK_50MHZ_period/2;
    end process;
 
    pckg_process: process
    begin
        iDATA_DEV           <= (others => '0');
        iSTART_PACK_DEV     <= '0';
        wait until start_pckg = '1';
        wait until iCLK_50MHZ = '1';
        iDATA_DEV <= x"7E";
        for i in 1 to 1498 loop 
            wait until iCLK_50MHZ = '1';
            iDATA_DEV    <= data_in;
            if data_in = x"7D" then
                data_in <= x"7F";
            else
                data_in <= data_in + '1';
            end if;
        end loop;
        wait until iCLK_50MHZ = '1';
        iDATA_DEV <= x"7E";
        wait until iCLK_50MHZ = '1';
        iDATA_DEV <= x"00";
        iSTART_PACK_DEV <= '1';
        wait until iCLK_50MHZ = '1';
        iSTART_PACK_DEV <= '0';
    end process;
 

   -- Stimulus process
   stim_proc: process
    begin		
        iRST                <= '1';           
        iRD_DATA_FAFO1      <= '0';
        
        wait for 500 ns;	
        wait until iCLK_50MHZ = '1';
        iRST <= '0';  
        wait until iCLK_50MHZ = '1';
        wait until iCLK_50MHZ = '1';
        wait until iCLK_50MHZ = '1';
        wait until iCLK_50MHZ = '1';
        start_pckg <= '1';
        wait until oPACK_RCV_FIFO1 = '1';
        start_pckg <= '0';
        wait until iCLK_50MHZ = '1';
        iRD_DATA_FAFO1 <= '1';
        
      wait;
   end process;

END;
