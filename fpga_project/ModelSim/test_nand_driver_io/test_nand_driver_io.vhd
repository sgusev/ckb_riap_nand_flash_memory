--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:16:54 05/29/2013
-- Design Name:   
-- Module Name:   E:/work/CKB_RIAP/nand_flash_memory/fpga_project/xilinx/test_nand_driver_io.vhd
-- Project Name:  nand
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: nand_driver_io
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
    use IEEE.STD_LOGIC_ARITH.ALL; 
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_nand_driver_io IS

    -- PORT(
         -- iCLK_50MHZ              : IN  std_logic;
         -- iRST              : IN  std_logic;
         -- iCOMMAND_DATA     : IN  std_logic;
         -- iSET_FEATURES     : IN  std_logic;
         -- iCOMMAND          : IN  std_logic_vector(7 downto 0);
         -- iRD               : IN  std_logic;
         -- iWR               : IN  std_logic;
         -- iADDR             : IN  std_logic_vector(15 downto 0);
         -- iDATA             : IN  std_logic_vector(7 downto 0);
         -- oDATA             : OUT  std_logic_vector(7 downto 0);
         -- oDATA_RDY_WR_RD   : OUT  std_logic;
         -- oLD               : OUT  std_logic;
         -- oTRANSMIT_COMMAND : OUT  std_logic;
         -- oSET_FEATURES     : OUT  std_logic;
-- --         oCLE              : OUT  std_logic;
-- --         oCE               : OUT  std_logic_vector(7 downto 0);
-- --         oWE               : OUT  std_logic;
-- --         oALE              : OUT  std_logic;
-- --         oRE               : OUT  std_logic;
-- --         oWP               : OUT  std_logic;
         -- oRB               : OUT  std_logic
        -- );

END test_nand_driver_io;
 
ARCHITECTURE behavior OF test_nand_driver_io IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT nand_driver_io
    PORT(
         iCLK_50MHZ        : IN  std_logic;
         iCLK_100MHZ        : IN  std_logic;
         iRST              : IN  std_logic;
         iCOMMAND_DATA     : IN  std_logic;
         iSET_FEATURES     : IN  std_logic;
         iCOMMAND          : IN  std_logic_vector(7 downto 0);
         iRD               : IN  std_logic;
         iWR               : IN  std_logic;
         iTRANSMIT_RECEIVE : IN  std_logic;
         iRB               : IN  std_logic;
         iADDR             : IN  std_logic_vector(35 downto 0);
         iDATA             : IN  std_logic_vector(7 downto 0);
         oDATA             : OUT  std_logic_vector(7 downto 0);
         oDATA_RDY_WR_RD   : OUT  std_logic;
         oLD               : OUT  std_logic;
         oTRANSMIT_COMMAND : OUT  std_logic;
         oSET_FEATURES     : OUT  std_logic;
         oCLE              : OUT  std_logic;
         oCE               : OUT  std_logic_vector(7 downto 0);
         oWE               : OUT  std_logic;
         oALE              : OUT  std_logic;
         oRE               : OUT  std_logic;
         oWP               : OUT  std_logic;
         oRB               : OUT  std_logic;
         ioDQ              : INOUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    
    COMPONENT nand_model
	PORT(
		Cle      : IN std_logic;
		Ale      : IN std_logic;
		Clk_We_n : IN std_logic;
		Wr_Re_n  : IN std_logic;
		Ce_n     : IN std_logic; --IN std_logic
		Ce2_n    : IN std_logic; --IN std_logic
		Wp_n     : IN std_logic;    
		Dq_Io    : INOUT std_logic_vector(7 downto 0); --0 to 0
		Dqs      : INOUT std_logic;      
		Rb_n     : OUT std_logic
		);
	END COMPONENT;
    

   --Inputs
   signal iCLK_50MHZ : std_logic := '0';
   signal iCLK_100MHZ : std_logic := '0';
   signal iRST : std_logic := '0';
   signal iCOMMAND_DATA : std_logic := '0';
   signal iSET_FEATURES : std_logic := '0';
   signal iCOMMAND : std_logic_vector(7 downto 0) := (others => '0');
   signal iRD : std_logic := '0';
   signal iWR : std_logic := '0';
   signal iTRANSMIT_RECEIVE : std_logic := '0';
   signal iADDR : std_logic_vector(35 downto 0) := (others => '0');
   signal iDATA : std_logic_vector(7 downto 0) := (others => '0');

	--BiDirs
   signal ioDQ : std_logic_vector(7 downto 0);
   signal ioDQs_s : std_logic;

 	--Outputs
   signal oDATA : std_logic_vector(7 downto 0);
   signal oDATA_RDY_WR_RD : std_logic;
   signal oLD : std_logic;
   signal oTRANSMIT_COMMAND : std_logic;
   signal oSET_FEATURES : std_logic;
   signal oCLE : std_logic;
   signal oCE : std_logic_vector(7 downto 0);
   signal oWE : std_logic;
   signal oALE : std_logic;
   signal oRE : std_logic;
   signal oWP : std_logic;
   signal oRB : std_logic;
   signal iRB : std_logic;
   
   signal data_in : std_logic_vector(7 downto 0) := "00000001";
   signal count_read : std_logic_vector(31 downto 0) := (others => '0');

   -- Clock period definitions
   constant iCLK_50MHZ_period : time := 20 ns;
   constant iCLK_100MHZ_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: nand_driver_io PORT MAP (
          iCLK_50MHZ              => iCLK_50MHZ,
          iCLK_100MHZ              => iCLK_100MHZ,
          iRST              => iRST,
          iCOMMAND_DATA     => iCOMMAND_DATA,
          iSET_FEATURES     => iSET_FEATURES,
          iCOMMAND          => iCOMMAND,
          iRD               => iRD,
          iWR               => iWR,
          iTRANSMIT_RECEIVE => iTRANSMIT_RECEIVE,
          iRB               => iRB,
          iADDR             => iADDR,
          iDATA             => iDATA,
          oDATA             => oDATA,
          oDATA_RDY_WR_RD   => oDATA_RDY_WR_RD,
          oLD               => oLD,
          oTRANSMIT_COMMAND => oTRANSMIT_COMMAND,
          oSET_FEATURES     => oSET_FEATURES,
          oCLE              => oCLE,
          oCE               => oCE,
          oWE               => oWE,
          oALE              => oALE,
          oRE               => oRE,
          oWP               => oWP,
          oRB               => oRB,
          ioDQ              => ioDQ
        );
        
    Inst_nand_model_0: nand_model PORT MAP(
		Dq_Io    => ioDQ(7 downto 0),
		Cle      => oCLE,
		Ale      => oALE,
		Clk_We_n => oWE,
		Wr_Re_n  => oRE,
		Dqs      => ioDQs_s,
		Ce_n     => oCE(0),
		Ce2_n    => oCE(1),
		Wp_n     => oWP,
		Rb_n     => iRB
	);
    
    Inst_nand_model_1: nand_model PORT MAP(
		Dq_Io    => ioDQ(7 downto 0),
		Cle      => oCLE,
		Ale      => oALE,
		Clk_We_n => oWE,
		Wr_Re_n  => oRE,
		Dqs      => ioDQs_s,
		Ce_n     => oCE(2),
		Ce2_n    => oCE(3),
		Wp_n     => oWP,
		Rb_n     => iRB
	);

    Inst_nand_model_2: nand_model PORT MAP(
		Dq_Io    => ioDQ(7 downto 0),
		Cle      => oCLE,
		Ale      => oALE,
		Clk_We_n => oWE,
		Wr_Re_n  => oRE,
		Dqs      => ioDQs_s,
		Ce_n     => oCE(4),
		Ce2_n    => oCE(5),
		Wp_n     => oWP,
		Rb_n     => iRB
	);

    Inst_nand_model_3: nand_model PORT MAP(
		Dq_Io    => ioDQ(7 downto 0),
		Cle      => oCLE,
		Ale      => oALE,
		Clk_We_n => oWE,
		Wr_Re_n  => oRE,
		Dqs      => ioDQs_s,
		Ce_n     => oCE(6),
		Ce2_n    => oCE(7),
		Wp_n     => oWP,
		Rb_n     => iRB
	);    
    

   -- Clock 50MHz new 
   iCLK_50MHz_process :process
   begin
		iCLK_50MHZ <= '0';
		wait for iCLK_50MHZ_period/2;
		iCLK_50MHZ <= '1';
		wait for iCLK_50MHZ_period/2;
   end process;
   
   -- Clock 100MHz  
   iCLK_100MHz_process :process
   begin
		iCLK_100MHZ <= '0';
		wait for iCLK_100MHZ_period/2;
		iCLK_100MHZ <= '1';
		wait for iCLK_100MHZ_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		     
        iRST          <= '1';   
        iCOMMAND_DATA <= '0';
        iSET_FEATURES <= '0';
        iCOMMAND      <= (others => '0');
        iRD           <= '0';
        iWR           <= '0';
        iADDR         <= (others => '0');
        iDATA         <= (others => '0');
        
        wait for 100 ns;	

        ----reset----
        wait until iCLK_50MHZ = '1';
        iRST          <= '0'; 
        ----
        
        ----reset command----
        wait until oRB = 'H';
        wait for iCLK_50MHZ_period*10;
        wait until iCLK_50MHZ = '1';
        iWR <= '1';
        iCOMMAND_DATA <= '0';
        iSET_FEATURES <= '0';
        iCOMMAND <= X"FF";
        wait until iCLK_50MHZ = '1';
        iWR <= '0';
        iCOMMAND_DATA <= '0';
        iSET_FEATURES <= '0';
        iCOMMAND <= X"00";
        ----
        
        ----set features----
        wait until oRB = 'H';
        wait for iCLK_50MHZ_period*100;
        wait until iCLK_50MHZ = '1';
        iWR <= '1';
        iCOMMAND_DATA <= '0';
        iSET_FEATURES <= '1';
        iCOMMAND <= X"EF";
        iADDR    <= B"0000"&X"00000001";
        iDATA    <= X"05";
        wait until iCLK_50MHZ = '1';
        iWR <= '0';
        iCOMMAND_DATA <= '0';
        iSET_FEATURES <= '0';
        iCOMMAND <= X"00";        
        ----
        
        -- --write----
        wait until oRB = 'H';
        wait for iCLK_50MHZ_period*10;
        wait until iCLK_50MHZ = '1';
        iWR <= '1';
        iCOMMAND_DATA <= '1';
        iSET_FEATURES <= '0';
        iTRANSMIT_RECEIVE <= '1';
        iADDR    <= B"0000"&X"00000000";
        iDATA    <= (others => '0');
        wait until iCLK_50MHZ = '1';
        iWR <= '0';        
        iCOMMAND_DATA <= '0';
        iSET_FEATURES <= '0';
        iADDR    <= B"0000"&X"00000000";      
        wait until oDATA_RDY_WR_RD = '1';
        wait until iCLK_50MHZ = '1';
        wait until iCLK_50MHZ = '1';

        for i in 8300 downto 1 loop 
            wait until iCLK_50MHZ = '1';
            iDATA    <= data_in;
            data_in <= data_in + '1';
            if i = 1 then
                iTRANSMIT_RECEIVE <= '0';    
            end if;
        end loop;
        
        wait until iCLK_50MHZ = '1';        
        
        -- --read----
        wait until oRB = 'H';
        wait for iCLK_50MHZ_period*10;
        wait until iCLK_50MHZ = '1';        
        iRD <= '1';
        iCOMMAND_DATA <= '1';
        iSET_FEATURES <= '0';
        iTRANSMIT_RECEIVE <= '1';
        iADDR    <= B"0000"&X"00000000";
        iDATA    <= (others => '0');
        wait until iCLK_50MHZ = '1';
        iRD <= '0';        
        iCOMMAND_DATA <= '0';
        iSET_FEATURES <= '0';
        iADDR    <= B"0000"&X"00000000";      
        wait until oDATA_RDY_WR_RD = '1';
        for i in 8300 downto 1 loop 
            wait until iCLK_50MHZ = '1';
            if i = 5 then
                iTRANSMIT_RECEIVE <= '0';              
            end if;
            --count_read <= count_read + '1';
        end loop;
        wait until iCLK_50MHZ = '1';        
      
        
--        iADDR    <= B"0000"&X"0000000";
--       iDATA    <= X"CC";
        
        ----

      -- insert stimulus here 

      wait;
   end process;

END;
