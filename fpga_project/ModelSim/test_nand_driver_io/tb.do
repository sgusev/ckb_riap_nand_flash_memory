#########################################################################################
#
#   Disclaimer   This software code and all associated documentation, comments or other 
#  of Warranty:  information (collectively "Software") is provided "AS IS" without 
#                warranty of any kind. MICRON TECHNOLOGY, INC. ("MTI") EXPRESSLY 
#                DISCLAIMS ALL WARRANTIES EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
#                TO, NONINFRINGEMENT OF THIRD PARTY RIGHTS, AND ANY IMPLIED WARRANTIES 
#                OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE. MTI DOES NOT 
#                WARRANT THAT THE SOFTWARE WILL MEET YOUR REQUIREMENTS, OR THAT THE 
#                OPERATION OF THE SOFTWARE WILL BE UNINTERRUPTED OR ERROR-FREE. 
#                FURTHERMORE, MTI DOES NOT MAKE ANY REPRESENTATIONS REGARDING THE USE OR 
#                THE RESULTS OF THE USE OF THE SOFTWARE IN TERMS OF ITS CORRECTNESS, 
#                ACCURACY, RELIABILITY, OR OTHERWISE. THE ENTIRE RISK ARISING OUT OF USE 
#                OR PERFORMANCE OF THE SOFTWARE REMAINS WITH YOU. IN NO EVENT SHALL MTI, 
#                ITS AFFILIATED COMPANIES OR THEIR SUPPLIERS BE LIABLE FOR ANY DIRECT, 
#                INDIRECT, CONSEQUENTIAL, INCIDENTAL, OR SPECIAL DAMAGES (INCLUDING, 
#                WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, BUSINESS INTERRUPTION, 
#                OR LOSS OF INFORMATION) ARISING OUT OF YOUR USE OF OR INABILITY TO USE 
#                THE SOFTWARE, EVEN IF MTI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
#                DAMAGES. Because some jurisdictions prohibit the exclusion or 
#                limitation of liability for consequential or incidental damages, the 
#                above limitation may not apply to you.
#
#                Copyright 2003 Micron Technology, Inc. All rights reserved.
#
#########################################################################################

# vlog  ../../src/l74a_nand_model/nand_defines.vh
# vlog  ../../src/l74a_nand_model/nand_parameters.vh

# vlog +define+V33 ../../src/l74a_nand_model/nand_die_model.v
# vlog +define+x8 ../../src/l74a_nand_model/nand_die_model.v
# vlog +define+CLASSF ../../src/l74a_nand_model/nand_die_model.v

# vlog +define+V33 nand_die_model.v
# vlog +define+x8 nand_die_model.v
# vlog +define+CLASSF nand_die_model.v


# vlog +define+FullMem nand_die_model.v
# vlog +define+SHORT_RESET nand_die_model.v


# vlog  -novopt nand_model.v nand_die_model.v test_nand_driver_io.vhd nand_driver_io.vhd nand_driver.vhd
# vsim  -novopt test_nand_driver_io

# vsim -GNUM_ROW=60680 test_nand_driver_io
vsim -novopt -GNUM_ROW=15170 -t 1ns test_nand_driver_io

add wave test_nand_driver_io/*
run 5ms -all
