--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:55:41 06/26/2013
-- Design Name:   
-- Module Name:   E:/work/CKB_RIAP/nand_flash_memory/fpga_project/xilinx/test_top_nand.vhd
-- Project Name:  nand
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: top_nand
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
    use IEEE.STD_LOGIC_ARITH.ALL; 
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_top_nand IS
END test_top_nand;
 
ARCHITECTURE behavior OF test_top_nand IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT top_nand
	PORT(
		iCLK_50MHZ        : IN std_logic;
		iCLK_100MHZ       : IN std_logic;
		iRST              : IN std_logic;
		iDATA_DEV         : IN std_logic_vector(7 downto 0);
		iSTART_PACK_DEV   : IN std_logic;
		iWRDREQ_DEV       : IN std_logic;
		iWRFULL_ETH       : IN std_logic;
		iWR_EMPT_BYTE_ETH : IN std_logic_vector(12 downto 0);
		iRB_NAND          : IN std_logic;    
		ioDQ_NAND         : INOUT std_logic_vector(7 downto 0);      
		oSTART_PACK_ETH   : OUT std_logic;
		oWR_DATA_ETH      : OUT std_logic_vector(7 downto 0);
		oWRREQ_ETH        : OUT std_logic;
		oCLE_NAND         : OUT std_logic;
		oCE_NAND          : OUT std_logic_vector(7 downto 0);
		oWE_NAND          : OUT std_logic;
		oALE_NAND         : OUT std_logic;
		oRE_NAND          : OUT std_logic;
		oWP_NAND          : OUT std_logic;
		oRB_NAND          : OUT std_logic;
		oDQS_NAND         : OUT std_logic;
        oDONE_RESET       : OUT std_logic;
        oDONE_SET_FEATURES: OUT std_logic
		);
	END COMPONENT;
    
    
    
    COMPONENT nand_model
	PORT(
		Cle      : IN std_logic;
		Ale      : IN std_logic;
		Clk_We_n : IN std_logic;
		Wr_Re_n  : IN std_logic;
		Ce_n     : IN std_logic; --IN std_logic
		Ce2_n    : IN std_logic; --IN std_logic
		Wp_n     : IN std_logic;    
		Dq_Io    : INOUT std_logic_vector(7 downto 0); --0 to 0
		Dqs      : INOUT std_logic;      
		Rb_n     : OUT std_logic
		);
	END COMPONENT;
    

    --Inputs
    signal iCLK_50MHZ           : std_logic := '0';
    signal iCLK_100MHZ          : std_logic := '0';
    signal iRST                 : std_logic := '0';
    signal iDATA_DEV            : std_logic_vector(7 downto 0) := (others => '0');
    signal iSTART_PACK_DEV      : std_logic := '0';
    signal iWRDREQ_DEV          : std_logic := '0';
    signal iWRFULL_ETH          : std_logic := '0';
    signal iWR_EMPT_BYTE_ETH    : std_logic_vector(12 downto 0) := (others => '0');
    signal iRB                  : std_logic := '0';

 	--Outputs
   signal oSTART_PACK_ETH : std_logic;
   signal oWR_DATA_ETH : std_logic_vector(7 downto 0);
   signal oWRREQ_ETH : std_logic;

   signal oCLE         : std_logic;
   signal oCE          : std_logic_vector(7 downto 0);
   signal oWE          : std_logic;
   signal oALE         : std_logic;
   signal oRE          : std_logic;
   signal oWP          : std_logic;
   signal oRB          : std_logic;
   signal oDQS         : std_logic;
   signal oDONE_RESET  : std_logic;
   signal oDONE_SET_FEATURES  : std_logic;
   
	--BiDirs
   signal ioDQ : std_logic_vector(7 downto 0);
   signal ioDQs_s : std_logic;
   
    --user signals
    signal data_in                  : std_logic_vector(7 downto 0) := "00000000";
    signal count_data_wr_s          : std_logic_vector(31 downto 0) := x"00000000";
    signal start_pckg               : std_logic := '0';
 
   constant iCLK_50MHZ_period : time := 20 ns;
   constant iCLK_100MHZ_period : time := 10 ns;
 
BEGIN
 
	Inst_top_nand: top_nand PORT MAP(
		iCLK_50MHZ          => iCLK_50MHZ,
		iCLK_100MHZ         => iCLK_100MHZ,
		iRST                => iRST,
		iDATA_DEV           => iDATA_DEV,
		iSTART_PACK_DEV     => iSTART_PACK_DEV,
		iWRDREQ_DEV         => iWRDREQ_DEV,
		iWRFULL_ETH         => iWRFULL_ETH,
		iWR_EMPT_BYTE_ETH   => iWR_EMPT_BYTE_ETH,
		iRB_NAND            => iRB,
		oSTART_PACK_ETH     => oSTART_PACK_ETH,
		oWR_DATA_ETH        => oWR_DATA_ETH,
		oWRREQ_ETH          => oWRREQ_ETH,
		oCLE_NAND           => oCLE,
		oCE_NAND            => oCE ,
		oWE_NAND            => oWE ,
		oALE_NAND           => oALE,
		oRE_NAND            => oRE ,
		oWP_NAND            => oWP ,
		oRB_NAND            => oRB ,
		oDQS_NAND           => oDQS,
		ioDQ_NAND           => ioDQ,
        oDONE_RESET         => oDONE_RESET,
        oDONE_SET_FEATURES  => oDONE_SET_FEATURES
	);
  
    Inst_nand_model_0: nand_model PORT MAP(
		Dq_Io    => ioDQ(7 downto 0),
		Cle      => oCLE,
		Ale      => oALE,
		Clk_We_n => oWE,
		Wr_Re_n  => oRE,
		Dqs      => ioDQs_s,
		Ce_n     => oCE(0),
		Ce2_n    => oCE(1),
		Wp_n     => oWP,
		Rb_n     => iRB
	);
    
    Inst_nand_model_1: nand_model PORT MAP(
		Dq_Io    => ioDQ(7 downto 0),
		Cle      => oCLE,
		Ale      => oALE,
		Clk_We_n => oWE,
		Wr_Re_n  => oRE,
		Dqs      => ioDQs_s,
		Ce_n     => oCE(2),
		Ce2_n    => oCE(3),
		Wp_n     => oWP,
		Rb_n     => iRB
	);

    Inst_nand_model_2: nand_model PORT MAP(
		Dq_Io    => ioDQ(7 downto 0),
		Cle      => oCLE,
		Ale      => oALE,
		Clk_We_n => oWE,
		Wr_Re_n  => oRE,
		Dqs      => ioDQs_s,
		Ce_n     => oCE(4),
		Ce2_n    => oCE(5),
		Wp_n     => oWP,
		Rb_n     => iRB
	);

    Inst_nand_model_3: nand_model PORT MAP(
		Dq_Io    => ioDQ(7 downto 0),
		Cle      => oCLE,
		Ale      => oALE,
		Clk_We_n => oWE,
		Wr_Re_n  => oRE,
		Dqs      => ioDQs_s,
		Ce_n     => oCE(6),
		Ce2_n    => oCE(7),
		Wp_n     => oWP,
		Rb_n     => iRB
	);    


    -- Clock process definitions
   iCLK_50MHZ_process :process
   begin
		iCLK_50MHZ <= '0';
		wait for iCLK_50MHZ_period/2;
		iCLK_50MHZ <= '1';
		wait for iCLK_50MHZ_period/2;
   end process;
   
   -- Clock 100MHz  
   iCLK_100MHz_process :process
   begin
		iCLK_100MHZ <= '0';
		wait for iCLK_100MHZ_period/2;
		iCLK_100MHZ <= '1';
		wait for iCLK_100MHZ_period/2;
   end process;
   
    pckg_process: process
    begin
        iDATA_DEV           <= (others => '0');
        iSTART_PACK_DEV     <= '0';
        wait until start_pckg = '1';
        wait until iCLK_50MHZ = '1';
        iDATA_DEV <= x"7E";
        for i in 1 to 1498 loop 
            wait until iCLK_50MHZ = '1';
            iDATA_DEV    <= data_in;
            if data_in = x"7D" then
                data_in <= x"7F";
            else
                data_in <= data_in + '1';
            end if;
        end loop;
        wait until iCLK_50MHZ = '1';
        iDATA_DEV <= x"7E";
        wait until iCLK_50MHZ = '1';
        iDATA_DEV <= x"00";
        iSTART_PACK_DEV <= '1';
        wait until iCLK_50MHZ = '1';
        iSTART_PACK_DEV <= '0';
    end process;
    
    gen_process: process
    begin
        --if oDONE_RESET = '1' then
            start_pckg <= '0' and oDONE_RESET and oDONE_SET_FEATURES;
            wait for 40 us;
            start_pckg <= '1' and oDONE_RESET and oDONE_SET_FEATURES;
            wait for 1 us;
        --end if;
    end process;
    
    count_data_wr: process
    begin
        wait until oWE = '1';
        count_data_wr_s <= count_data_wr_s + '1';
    end process;
    
   -- Stimulus process
   stim_proc: process
   begin		
        iRST <= '1';                  
        iWRFULL_ETH <= '0';     
        iWR_EMPT_BYTE_ETH <= (others => '1');
        wait for 500 ns;	
        wait until iCLK_50MHZ = '1';
        iRST <= '0';  
        
        iWRFULL_ETH <= '1';
        wait until iCLK_50MHZ = '1';
        wait until iCLK_50MHZ = '1';
        wait until iCLK_50MHZ = '1';
        wait until iCLK_50MHZ = '1';
        
        -- start_pckg <= '1';
        -- wait until iCLK_50MHZ = '1';
        -- start_pckg <= '0';

      wait;
   end process;

END;
