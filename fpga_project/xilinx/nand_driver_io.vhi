
-- VHDL Instantiation Created from source file nand_driver_io.vhd -- 14:52:12 06/28/2013
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT nand_driver_io
	PORT(
		iCLK_50MHZ : IN std_logic;
		iCLK_100MHZ : IN std_logic;
		iRST : IN std_logic;
		iCOMMAND_DATA : IN std_logic;
		iSET_FEATURES : IN std_logic;
		iCOMMAND : IN std_logic_vector(7 downto 0);
		iRD : IN std_logic;
		iWR : IN std_logic;
		iTRANSMIT_RECEIVE : IN std_logic;
		iADDR : IN std_logic_vector(35 downto 0);
		iDATA : IN std_logic_vector(7 downto 0);
		iRB : IN std_logic;    
		ioDQ : INOUT std_logic_vector(7 downto 0);      
		oDATA : OUT std_logic_vector(7 downto 0);
		oDATA_RDY_WR_RD : OUT std_logic;
		oLD : OUT std_logic;
		oTRANSMIT_COMMAND : OUT std_logic;
		oSET_FEATURES : OUT std_logic;
		oCLE : OUT std_logic;
		oCE : OUT std_logic_vector(7 downto 0);
		oWE : OUT std_logic;
		oALE : OUT std_logic;
		oRE : OUT std_logic;
		oWP : OUT std_logic;
		oRB : OUT std_logic
		);
	END COMPONENT;

	Inst_nand_driver_io: nand_driver_io PORT MAP(
		iCLK_50MHZ => ,
		iCLK_100MHZ => ,
		iRST => ,
		iCOMMAND_DATA => ,
		iSET_FEATURES => ,
		iCOMMAND => ,
		iRD => ,
		iWR => ,
		iTRANSMIT_RECEIVE => ,
		iADDR => ,
		iDATA => ,
		iRB => ,
		oDATA => ,
		oDATA_RDY_WR_RD => ,
		oLD => ,
		oTRANSMIT_COMMAND => ,
		oSET_FEATURES => ,
		oCLE => ,
		oCE => ,
		oWE => ,
		oALE => ,
		oRE => ,
		oWP => ,
		oRB => ,
		ioDQ => 
	);


