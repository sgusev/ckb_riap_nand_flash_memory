
-- VHDL Instantiation Created from source file top_nand.vhd -- 22:05:52 07/02/2013
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT top_nand
	PORT(
		iCLK_50MHZ : IN std_logic;
		iCLK_100MHZ : IN std_logic;
		iRST : IN std_logic;
		iDATA_DEV : IN std_logic_vector(7 downto 0);
		iSTART_PACK_DEV : IN std_logic;
		iWRDREQ_DEV : IN std_logic;
		iWRFULL_ETH : IN std_logic;
		iWR_EMPT_BYTE_ETH : IN std_logic_vector(12 downto 0);
		iRB_NAND : IN std_logic;    
		ioDQ_NAND : INOUT std_logic_vector(7 downto 0);      
		oSTART_PACK_ETH : OUT std_logic;
		oWR_DATA_ETH : OUT std_logic_vector(7 downto 0);
		oWRREQ_ETH : OUT std_logic;
		oCLE_NAND : OUT std_logic;
		oCE_NAND : OUT std_logic_vector(7 downto 0);
		oWE_NAND : OUT std_logic;
		oALE_NAND : OUT std_logic;
		oRE_NAND : OUT std_logic;
		oWP_NAND : OUT std_logic;
		oRB_NAND : OUT std_logic;
		oDQS_NAND : OUT std_logic
		);
	END COMPONENT;

	Inst_top_nand: top_nand PORT MAP(
		iCLK_50MHZ => ,
		iCLK_100MHZ => ,
		iRST => ,
		iDATA_DEV => ,
		iSTART_PACK_DEV => ,
		iWRDREQ_DEV => ,
		iWRFULL_ETH => ,
		iWR_EMPT_BYTE_ETH => ,
		iRB_NAND => ,
		oSTART_PACK_ETH => ,
		oWR_DATA_ETH => ,
		oWRREQ_ETH => ,
		oCLE_NAND => ,
		oCE_NAND => ,
		oWE_NAND => ,
		oALE_NAND => ,
		oRE_NAND => ,
		oWP_NAND => ,
		oRB_NAND => ,
		oDQS_NAND => ,
		ioDQ_NAND => 
	);


