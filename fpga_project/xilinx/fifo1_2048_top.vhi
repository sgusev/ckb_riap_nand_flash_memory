
-- VHDL Instantiation Created from source file fifo1_2048_top.vhd -- 15:33:40 06/26/2013
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT fifo1_2048_top
	PORT(
		iCLK_50MHZ : IN std_logic;
		iRST : IN std_logic;
		iDATA_DEV : IN std_logic_vector(7 downto 0);
		iSTART_PACK_DEV : IN std_logic;
		iWRDREQ_DEV : IN std_logic;
		iRD_DATA_FAFO1 : IN std_logic;          
		oEMPTY_FIFO1 : OUT std_logic;
		oFULL_FIFO1 : OUT std_logic;
		oVALID_FIFO1 : OUT std_logic;
		oDATA_FIFO1 : OUT std_logic_vector(7 downto 0);
		oCOUNT_PACK_FIFO1 : OUT std_logic_vector(10 downto 0);
		oPACK_RCV_FIFO1 : OUT std_logic
		);
	END COMPONENT;

	Inst_fifo1_2048_top: fifo1_2048_top PORT MAP(
		iCLK_50MHZ => ,
		iRST => ,
		iDATA_DEV => ,
		iSTART_PACK_DEV => ,
		iWRDREQ_DEV => ,
		iRD_DATA_FAFO1 => ,
		oEMPTY_FIFO1 => ,
		oFULL_FIFO1 => ,
		oVALID_FIFO1 => ,
		oDATA_FIFO1 => ,
		oCOUNT_PACK_FIFO1 => ,
		oPACK_RCV_FIFO1 => 
	);


