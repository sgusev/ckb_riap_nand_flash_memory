
-- VHDL Instantiation Created from source file fifo_generator_v8_2.vhd -- 14:56:49 06/26/2013
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT fifo_generator_v8_2
	PORT(
		clk : IN std_logic;
		srst : IN std_logic;
		din : IN std_logic_vector(7 downto 0);
		wr_en : IN std_logic;
		rd_en : IN std_logic;          
		dout : OUT std_logic_vector(7 downto 0);
		full : OUT std_logic;
		empty : OUT std_logic;
		valid : OUT std_logic;
		data_count : OUT std_logic_vector(10 downto 0)
		);
	END COMPONENT;

	Inst_fifo_generator_v8_2: fifo_generator_v8_2 PORT MAP(
		clk => ,
		srst => ,
		din => ,
		wr_en => ,
		rd_en => ,
		dout => ,
		full => ,
		empty => ,
		valid => ,
		data_count => 
	);


