----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:32:07 06/26/2013 
-- Design Name: 
-- Module Name:    top_nand - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
    use IEEE.STD_LOGIC_ARITH.ALL;  

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_nand is
    GENERIC( 
        WIDTH_DATA                  : integer := 8;   
        WIDTH_DATA_COUNT_FIFO1      : integer := 11;   
        WIDTH_DATA_COUNT_FIFO23     : integer := 13;   
        N                           : integer := 13;
        WIDTH_CE                    : integer := 8;        
        WIDTH_COMMAND               : integer := 8;        
        WIDTH_ADDR_NAND_DR          : integer := 36        
        );
    PORT(
		iCLK_50MHZ          : IN std_logic;
		iCLK_100MHZ         : IN std_logic;
		iRST                : IN std_logic;
        iDATA_DEV           : IN std_logic_vector((WIDTH_DATA-1) downto 0);
        iSTART_PACK_DEV     : IN std_logic;
        iWRDREQ_DEV         : IN std_logic;
        iWRFULL_ETH         : IN std_logic;
        iWR_EMPT_BYTE_ETH   : IN std_logic_vector((N-1) downto 0);       
        iRB_NAND            : IN std_logic;
        
        oSTART_PACK_ETH     : OUT std_logic;
        oWR_DATA_ETH        : OUT std_logic_vector((WIDTH_DATA-1) downto 0);
        oWRREQ_ETH          : OUT std_logic;
        oCLE_NAND           : OUT std_logic;
        oCE_NAND            : OUT std_logic_vector((WIDTH_CE-1) downto 0);
        oWE_NAND            : OUT std_logic;
        oALE_NAND           : OUT std_logic;
        oRE_NAND            : OUT std_logic;
        oWP_NAND            : OUT std_logic;
        oRB_NAND            : OUT std_logic;
        oDQS_NAND           : OUT std_logic;
        oDONE_RESET         : OUT std_logic;
        oDONE_SET_FEATURES  : OUT std_logic;
        
        ioDQ_NAND           : INOUT std_logic_vector((WIDTH_DATA-1) downto 0)
        );
end top_nand;

architecture Behavioral of top_nand is

	COMPONENT fifo1_2048_top
    GENERIC( 
        WIDTH_DATA_FIFO1  : integer := WIDTH_DATA;   
        WIDTH_DATA_COUNT  : integer := WIDTH_DATA_COUNT_FIFO1   
        );
	PORT(
		iCLK_50MHZ        : IN std_logic;
		iRST              : IN std_logic;
		iDATA_DEV         : IN std_logic_vector((WIDTH_DATA_FIFO1-1) downto 0);
		iSTART_PACK_DEV   : IN std_logic;
		iWRDREQ_DEV       : IN std_logic;
		iRD_DATA_FAFO1    : IN std_logic;          
		oEMPTY_FIFO1      : OUT std_logic;
		oFULL_FIFO1       : OUT std_logic;
		oVALID_FIFO1      : OUT std_logic;
		oDATA_FIFO1       : OUT std_logic_vector((WIDTH_DATA_FIFO1-1) downto 0);
		oCOUNT_PACK_FIFO1 : OUT std_logic_vector((WIDTH_DATA_COUNT-1) downto 0);
		oPACK_RCV_FIFO1   : OUT std_logic
		);
	END COMPONENT;
    
	COMPONENT fifo23_8192
	PORT(
		clk        : IN std_logic;
		srst       : IN std_logic;
		din        : IN std_logic_vector((WIDTH_DATA-1) downto 0);
		wr_en      : IN std_logic;
		rd_en      : IN std_logic;          
		dout       : OUT std_logic_vector((WIDTH_DATA-1) downto 0);
		full       : OUT std_logic;
		empty      : OUT std_logic;
		valid      : OUT std_logic;
		data_count : OUT std_logic_vector((WIDTH_DATA_COUNT_FIFO23-1) downto 0)
		);
	END COMPONENT;
    
    COMPONENT nand_driver_io
	GENERIC( 
        WIDTH_ADDR_NAND_DR  : integer := WIDTH_ADDR_NAND_DR;        --width addr bus
        WIDTH_DATA_NAND_DR  : integer := WIDTH_DATA;                --width data bus
        WIDTH_COMMAND       : integer := WIDTH_COMMAND;             --width command bus
        WIDTH_CE            : integer := WIDTH_CE                   --width CE bus
        );
    PORT(
		iCLK_50MHZ        : IN std_logic;
		iCLK_100MHZ       : IN std_logic;
		iRST              : IN std_logic;
		iCOMMAND_DATA     : IN std_logic;
		iSET_FEATURES     : IN std_logic;
		iCOMMAND          : IN std_logic_vector((WIDTH_COMMAND-1) downto 0);
		iRD               : IN std_logic;
		iWR               : IN std_logic;
		iTRANSMIT_RECEIVE : IN std_logic;
		iADDR             : IN std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0);
		iDATA             : IN std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);
		iRB               : IN std_logic;    
		ioDQ              : INOUT std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);      
		oDATA             : OUT std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);
		oDATA_RDY_WR_RD   : OUT std_logic;
		oLD               : OUT std_logic;
		oTRANSMIT_COMMAND : OUT std_logic;
		oSET_FEATURES     : OUT std_logic;
		oCLE              : OUT std_logic;
		oCE               : OUT std_logic_vector((WIDTH_CE-1) downto 0);
		oWE               : OUT std_logic;
		oALE              : OUT std_logic;
		oRE               : OUT std_logic;
		oWP               : OUT std_logic;
		oRB               : OUT std_logic
		);
	END COMPONENT;
        
    signal data_in_dev_s             : std_logic_vector((WIDTH_DATA-1) downto 0);
    signal data_out_fifo1_s          : std_logic_vector((WIDTH_DATA-1) downto 0);
    signal count_pack_fifo1_s        : std_logic_vector((WIDTH_DATA_COUNT_FIFO1-1) downto 0);
    signal start_pack_dev_s          : std_logic;
    signal wrdreq_dev_s              : std_logic;
    signal rd_data_fifo1_s           : std_logic;
    signal empty_fifo1_s             : std_logic;
    signal full_fifo1_s              : std_logic;
    signal valid_fifo1_s             : std_logic;
    signal pack_rcv_fifo1_s          : std_logic;

    signal wrfull_eth_s                     : std_logic;
    signal wr_empt_byte_eth                 : std_logic_vector((N-1) downto 0);

    signal data_in_fifo2_s          : std_logic_vector((WIDTH_DATA-1) downto 0);
    signal wr_en_fifo2_s            : std_logic;
    signal rd_en_fifo2_s            : std_logic;
    signal data_out_fifo2_s         : std_logic_vector((WIDTH_DATA-1) downto 0);
    signal full_fifo2_s             : std_logic;
    signal empty_fifo2_s            : std_logic;
    signal valid_fifo2_s            : std_logic;
    signal data_count_fifo2_s       : std_logic_vector((WIDTH_DATA_COUNT_FIFO23-1) downto 0);
    signal data_in_fifo3_s          : std_logic_vector((WIDTH_DATA-1) downto 0);
    signal wr_en_fifo3_s            : std_logic;
    signal rd_en_fifo3_s            : std_logic;
    signal data_out_fifo3_s         : std_logic_vector((WIDTH_DATA-1) downto 0);
    signal full_fifo3_s             : std_logic;
    signal empty_fifo3_s            : std_logic;
    signal valid_fifo3_s            : std_logic;
    signal data_count_fifo3_s       : std_logic_vector((WIDTH_DATA_COUNT_FIFO23-1) downto 0);

    --wr_to_eth
    signal rd_data_fifo1_wr_to_eth_s    : std_logic;
    signal ld_wr_to_eth                 : std_logic;         
    signal start_wr_to_eth_s            : std_logic;
    signal start_wr_to_eth_s1            : std_logic;
    signal wr_data_eth_wr_to_eth_s      : std_logic_vector((WIDTH_DATA-1) downto 0);
    signal start_pack_eth_wr_to_eth_s   : std_logic;

    --wr_to_fifo2
    signal start_wr_to_fifo2_s          : std_logic;
    signal start_wr_to_fifo2_s1         : std_logic;
    signal rd_data_fifo1_wr_to_fifo2_s  : std_logic;
    signal count_pack_wr_to_fifo2_s     : std_logic_vector((WIDTH_DATA_COUNT_FIFO1-1) downto 0);

    signal command_data_nand_dr_s       : std_logic ;
    signal set_features_in_nand_dr_s    : std_logic ;
    signal command_nand_dr_s            : std_logic_vector((WIDTH_COMMAND-1) downto 0) ;
    signal rd_nand_dr_s                 : std_logic ;
    signal wr_nand_dr_s                 : std_logic ;
    signal transmit_receive_nand_dr_s   : std_logic ;
    signal addr_in_nand_dr_s            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) ;
    signal data_in_nand_dr_s            : std_logic_vector((WIDTH_DATA-1) downto 0) ;
    signal rb_in_nand_dr_s              : std_logic ;
    signal data_out_nand_dr_s           : std_logic_vector((WIDTH_DATA-1) downto 0) ;
    signal data_rdy_wr_rd_nand_dr_s     : std_logic ;
    signal ld_nand_dr_s                 : std_logic ;
    signal transmit_command_nand_dr_s   : std_logic ;
    signal set_features_out_nand_dr_s   : std_logic ;
    signal cle_nand_dr_s                : std_logic ;
    signal ce_nand_dr_s                 : std_logic_vector((WIDTH_CE-1) downto 0) ;
    signal we_nand_dr_s                 : std_logic ;
    signal ale_nand_dr_s                : std_logic ;
    signal re_nand_dr_s                 : std_logic ;
    signal wp_nand_dr_s                 : std_logic ;
    signal rb_out_nand_dr_s             : std_logic ;
    signal dq_inout_nand_dr_s           : std_logic_vector((WIDTH_DATA-1) downto 0) ;
    
    --addr_wr_to_nand
    signal addr_wr_to_nand_inv_s        : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0);     
    signal addr_wr_to_nand_s            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0);     
    
    --wr_to_nand
    signal data_free_count_fifo2             : std_logic_vector((WIDTH_DATA_COUNT_FIFO1-1) downto 0);
    signal start_wr_to_nand_s                : std_logic ;
    signal start_wr_to_nand_s1               : std_logic ;
    signal count_pack_wr_to_nand_s           : std_logic_vector((WIDTH_DATA_COUNT_FIFO23-1) downto 0);                
    signal count_wr_to_nand_s                : std_logic_vector((WIDTH_DATA_COUNT_FIFO23) downto 0);                
    signal wr_nand_dr_wr_to_nand_s           : std_logic ;
    signal command_data_wr_to_nand_s         : std_logic ;
    signal transmit_receive_wr_to_nand_s     : std_logic ;    
    signal rd_en_fifo2_wr_to_nand_s          : std_logic ;    
    signal addr_in_wr_to_nand_s              : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0); 
    signal data_in_wr_to_nand_s              : std_logic_vector((WIDTH_DATA-1) downto 0) ;
    
    --rd_from_nand
    signal ess_rd_from_nand1_s                  : std_logic_vector(23 downto 0); 
    signal ess_rd_from_nand2_s                  : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand3_s                  : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand4_s                  : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand5_s                  : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand6_s                  : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand7_s                  : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand8_s                  : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand9_s                  : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand10_s                 : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand11_s                 : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand12_s                 : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand13_s                 : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand14_s                 : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand15_s                 : std_logic_vector(23 downto 0);
    signal ess_rd_from_nand16_s                 : std_logic_vector(23 downto 0);
    signal count_rd_from_nand1_s                : std_logic_vector(23 downto 0);
    signal count_rd_from_nand2_s                : std_logic_vector(23 downto 0);
    signal count_rd_from_nand3_s                : std_logic_vector(23 downto 0);
    signal count_rd_from_nand4_s                : std_logic_vector(23 downto 0);
    signal count_rd_from_nand5_s                : std_logic_vector(23 downto 0);
    signal count_rd_from_nand6_s                : std_logic_vector(23 downto 0);
    signal count_rd_from_nand7_s                : std_logic_vector(23 downto 0);
    signal count_rd_from_nand8_s                : std_logic_vector(23 downto 0);
    signal count_rd_from_nand9_s                : std_logic_vector(23 downto 0);
    signal count_rd_from_nand10_s               : std_logic_vector(23 downto 0);
    signal count_rd_from_nand11_s               : std_logic_vector(23 downto 0);
    signal count_rd_from_nand12_s               : std_logic_vector(23 downto 0);
    signal count_rd_from_nand13_s               : std_logic_vector(23 downto 0);
    signal count_rd_from_nand14_s               : std_logic_vector(23 downto 0);
    signal count_rd_from_nand15_s               : std_logic_vector(23 downto 0);
    signal count_rd_from_nand16_s               : std_logic_vector(23 downto 0);
    signal start_rd_from_nand_s                 : std_logic ; 
    signal start_rd_from_nand_s1                : std_logic ; 
    signal count_rd_from_nand_s                 : std_logic ; 
    
    --reset_nand
    signal done_reset_nand_s                     : std_logic ;    
    signal start_reset_nand_s                    : std_logic ;  
    signal count_reset_nand_s                    : std_logic_vector(1 downto 0) ;
    signal wr_nand_dr_reset_nand_s               : std_logic ;
    signal command_data_nand_dr_reset_nand_s     : std_logic ;
    signal set_features_in_reset_nand_s          : std_logic ; 
    signal command_reset_nand_s                  : std_logic_vector((WIDTH_COMMAND-1) downto 0) ;
    
    --set_features_nand
    signal done_set_features_nand_s            : std_logic ;
    signal start_set_features_nand_s           : std_logic ;
    signal count_set_features_nand_s           : std_logic_vector(1 downto 0);
    signal wr_nand_dr_set_features_nand_s      : std_logic ;
    signal command_data_set_features_nand_s    : std_logic ;
    signal set_features_in_set_features_nand_s : std_logic ;
    signal command_set_features_nand_s         : std_logic_vector((WIDTH_COMMAND-1) downto 0);
    signal addr_in_set_features_nand_s         : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0);          
    signal data_in_set_features_nand_s         : std_logic_vector((WIDTH_DATA-1) downto 0) ;             
    
    
    

begin

	Inst_fifo1_2048_top: fifo1_2048_top PORT MAP(
		iCLK_50MHZ        => iCLK_50MHZ,
		iRST              => iRST,
		iDATA_DEV         => data_in_dev_s,
		iSTART_PACK_DEV   => start_pack_dev_s,
		iWRDREQ_DEV       => wrdreq_dev_s,
		iRD_DATA_FAFO1    => rd_data_fifo1_s,
		oEMPTY_FIFO1      => empty_fifo1_s,
		oFULL_FIFO1       => full_fifo1_s,
		oVALID_FIFO1      => valid_fifo1_s,
		oDATA_FIFO1       => data_out_fifo1_s,
		oCOUNT_PACK_FIFO1 => count_pack_fifo1_s,
		oPACK_RCV_FIFO1   => pack_rcv_fifo1_s
	);

	Inst_fifo2_8192: fifo23_8192 PORT MAP(
		clk        => iCLK_50MHZ,
		srst       => iRST,
		din        => data_in_fifo2_s,
		wr_en      => wr_en_fifo2_s,
		rd_en      => rd_en_fifo2_s,
		dout       => data_out_fifo2_s,
		full       => full_fifo2_s,
		empty      => empty_fifo2_s,
		valid      => valid_fifo2_s,
		data_count => data_count_fifo2_s
	);    

	Inst_fifo3_8192: fifo23_8192 PORT MAP(
		clk        => iCLK_50MHZ,
		srst       => iRST,
		din        => data_in_fifo3_s,
		wr_en      => wr_en_fifo3_s,
		rd_en      => rd_en_fifo3_s,
		dout       => data_out_fifo3_s,
		full       => full_fifo3_s,
		empty      => empty_fifo3_s,
		valid      => valid_fifo3_s,
		data_count => data_count_fifo3_s
	);    

	Inst_nand_driver_io: nand_driver_io PORT MAP(
		iCLK_50MHZ        => iCLK_50MHZ,
		iCLK_100MHZ       => iCLK_100MHZ,
		iRST              => iRST,
		iCOMMAND_DATA     => command_data_nand_dr_s,
		iSET_FEATURES     => set_features_in_nand_dr_s,
		iCOMMAND          => command_nand_dr_s,
		iRD               => rd_nand_dr_s,
		iWR               => wr_nand_dr_s,
		iTRANSMIT_RECEIVE => transmit_receive_nand_dr_s,
		iADDR             => addr_in_nand_dr_s,
		iDATA             => data_in_nand_dr_s,
		iRB               => rb_in_nand_dr_s,
		oDATA             => data_out_nand_dr_s,
		oDATA_RDY_WR_RD   => data_rdy_wr_rd_nand_dr_s,
		oLD               => ld_nand_dr_s,
		oTRANSMIT_COMMAND => transmit_command_nand_dr_s,
		oSET_FEATURES     => set_features_out_nand_dr_s,
		oCLE              => cle_nand_dr_s,
		oCE               => ce_nand_dr_s,
		oWE               => we_nand_dr_s,
		oALE              => ale_nand_dr_s,
		oRE               => re_nand_dr_s,
		oWP               => wp_nand_dr_s,
		oRB               => rb_out_nand_dr_s,
		ioDQ              => ioDQ_NAND
	);    
    
    
    wr_to_eth: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then           
            if (iRST = '1') then 
                ld_wr_to_eth               <= '0';
                start_wr_to_eth_s          <= '0';
                start_wr_to_eth_s1          <= '0';
                rd_data_fifo1_wr_to_eth_s  <= '0';
                wr_data_eth_wr_to_eth_s    <= (others => '0'); 
                start_pack_eth_wr_to_eth_s <= '0';
            else
                start_wr_to_eth_s1 <= start_wr_to_eth_s;
                
                if pack_rcv_fifo1_s = '1' then
                    ld_wr_to_eth <= '1';
                    if wrfull_eth_s = '0' then
                        if (wr_empt_byte_eth((N-1) downto WIDTH_DATA_COUNT_FIFO1) > conv_std_logic_vector(0,(N-WIDTH_DATA_COUNT_FIFO1))) or (wr_empt_byte_eth((WIDTH_DATA_COUNT_FIFO1-1) downto 0) >= count_pack_fifo1_s((WIDTH_DATA_COUNT_FIFO1-1) downto 0)) then
                            start_wr_to_eth_s <= '1';
                            rd_data_fifo1_wr_to_eth_s <= '1';
                        end if;
                    end if;
                end if;
                
                if start_wr_to_eth_s1 = '1' then
                    if valid_fifo1_s = '1' then
                        wr_data_eth_wr_to_eth_s <= data_out_fifo1_s;
                    else
                        wr_data_eth_wr_to_eth_s <= (others => '0');
                        start_wr_to_eth_s <= '0';
                        rd_data_fifo1_wr_to_eth_s <= '0';
                        start_pack_eth_wr_to_eth_s <= '1';
                    end if;
                end if;
                
                if start_pack_eth_wr_to_eth_s = '1' then
                    start_pack_eth_wr_to_eth_s <= '0';
                end if;
                if ld_wr_to_eth = '1' then
                    ld_wr_to_eth <= '0';
                end if;
                
            end if;
        end if;
    end process wr_to_eth;
    
    wr_to_fifo2: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then           
            if (iRST = '1') then 
                data_in_fifo2_s <= (others => '0');
                wr_en_fifo2_s <= '0';
                start_wr_to_fifo2_s <= '0';
                start_wr_to_fifo2_s1 <= '0';
                rd_data_fifo1_wr_to_fifo2_s <= '0';
                count_pack_wr_to_fifo2_s <= (others => '0');
            else
                start_wr_to_fifo2_s1 <= start_wr_to_fifo2_s;
                
                if ld_wr_to_eth = '1' and start_wr_to_eth_s = '0' then
                    start_wr_to_fifo2_s <= '1';
                    rd_data_fifo1_wr_to_fifo2_s <= '1'; 
                    count_pack_wr_to_fifo2_s <= count_pack_fifo1_s;                    
                end if;
                
                if start_wr_to_fifo2_s1 = '1' then               
                    if valid_fifo1_s = '1' and count_pack_wr_to_fifo2_s > conv_std_logic_vector(0,(WIDTH_DATA_COUNT_FIFO1-1)) then
                        data_in_fifo2_s <= data_out_fifo1_s;
                        wr_en_fifo2_s <= '1';
                        count_pack_wr_to_fifo2_s <= count_pack_wr_to_fifo2_s - '1';
                        if count_pack_wr_to_fifo2_s = conv_std_logic_vector(2,(WIDTH_DATA_COUNT_FIFO1-1)) then
                            rd_data_fifo1_wr_to_fifo2_s <= '0';    
                        end if;
                    else                       
                        data_in_fifo2_s <= (others => '0');
                        wr_en_fifo2_s <= '0'; 
                        start_wr_to_fifo2_s <= '0';
                    end if;                    
                end if;
            end if;
        end if;
    end process wr_to_fifo2;
    
    reset_nand: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then           
            if (iRST = '1') then 
                done_reset_nand_s                 <= '0';
                start_reset_nand_s                <= '0';
                count_reset_nand_s                <= (others => '0');
                wr_nand_dr_reset_nand_s           <= '0';
                command_data_nand_dr_reset_nand_s <= '0';
                set_features_in_reset_nand_s      <= '0';
                command_reset_nand_s              <= (others => '0');
            else
                if done_reset_nand_s = '0' and rb_out_nand_dr_s = 'H' and start_reset_nand_s = '0' then
                    start_reset_nand_s <= '1';
                end if;
                if start_reset_nand_s = '1' then
                    if count_reset_nand_s = conv_std_logic_vector(0,2) then
                        wr_nand_dr_reset_nand_s           <= '1';
                        command_data_nand_dr_reset_nand_s <= '0';
                        set_features_in_reset_nand_s      <= '0';
                        command_reset_nand_s              <= x"FF";
                        count_reset_nand_s                <= count_reset_nand_s + '1';
                    elsif count_reset_nand_s = conv_std_logic_vector(1,2) then
                        wr_nand_dr_reset_nand_s           <= '0';
                        command_data_nand_dr_reset_nand_s <= '0';
                        set_features_in_reset_nand_s      <= '0';
                        command_reset_nand_s              <= x"00";
                        count_reset_nand_s                <= count_reset_nand_s + '1';
                    elsif count_reset_nand_s = conv_std_logic_vector(2,2) then
                        if rb_out_nand_dr_s = '0' then
                            count_reset_nand_s <= count_reset_nand_s + '1';
                        end if;
                    elsif count_reset_nand_s = conv_std_logic_vector(3,2) then
                        if rb_out_nand_dr_s = 'H' then
                            count_reset_nand_s <= conv_std_logic_vector(0,2);
                            start_reset_nand_s <= '0';
                            done_reset_nand_s  <= '1';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process reset_nand;
    
    set_features_nand: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then           
            if (iRST = '1') then 
                done_set_features_nand_s           <= '0';
                start_set_features_nand_s          <= '0';
                count_set_features_nand_s          <= (others => '0');
                wr_nand_dr_set_features_nand_s     <= '0';
                command_data_set_features_nand_s   <= '0';
                set_features_in_set_features_nand_s<= '0';
                command_set_features_nand_s        <= (others => '0');  
                addr_in_set_features_nand_s        <= (others => '0');   
                data_in_set_features_nand_s        <= (others => '0');   
            else
                if done_reset_nand_s = '1' and done_set_features_nand_s = '0' and start_set_features_nand_s = '0' then
                    start_set_features_nand_s <= '1';
                end if;
                if start_set_features_nand_s = '1' then
                    if count_set_features_nand_s = conv_std_logic_vector(0,2) then
                        wr_nand_dr_set_features_nand_s      <= '1';
                        command_data_set_features_nand_s    <= '0';
                        set_features_in_set_features_nand_s <= '1';
                        command_set_features_nand_s         <= x"EF";
                        addr_in_set_features_nand_s         <= conv_std_logic_vector(1,WIDTH_ADDR_NAND_DR);
                        data_in_set_features_nand_s         <= conv_std_logic_vector(5,WIDTH_DATA);
                        count_set_features_nand_s             <= count_set_features_nand_s + '1';
                    elsif count_set_features_nand_s = conv_std_logic_vector(1,2) then
                        wr_nand_dr_set_features_nand_s      <= '0';
                        command_data_set_features_nand_s    <= '0';
                        set_features_in_set_features_nand_s <= '0';
                        command_set_features_nand_s         <= x"00";
                        addr_in_set_features_nand_s         <= conv_std_logic_vector(0,WIDTH_ADDR_NAND_DR);
                        data_in_set_features_nand_s         <= conv_std_logic_vector(0,WIDTH_DATA);
                        count_set_features_nand_s             <= count_set_features_nand_s + '1';
                    elsif count_set_features_nand_s = conv_std_logic_vector(2,2) then
                        if rb_out_nand_dr_s = '0' then
                            count_set_features_nand_s <= count_set_features_nand_s + '1';
                        end if; 
                    elsif count_set_features_nand_s = conv_std_logic_vector(3,2) then
                        if rb_out_nand_dr_s = 'H' then
                            count_set_features_nand_s <= conv_std_logic_vector(0,2);
                            start_set_features_nand_s <= '0';
                            done_set_features_nand_s <= '1';
                        end if;                    
                    end if;
                end if;
            end if;
        end if;
    end process set_features_nand;
    
    addr_wr_to_nand: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then           
            if (iRST = '1') then 
                addr_wr_to_nand_s       <= (others => '0');
                addr_wr_to_nand_inv_s   <= (others => '0');
            else
                if start_wr_to_nand_s1 = '1' and start_wr_to_nand_s = '0' then
                    addr_wr_to_nand_s <= addr_wr_to_nand_s + '1';
                end if;
                
                addr_wr_to_nand_inv_s(33) <= addr_wr_to_nand_s(0);
                addr_wr_to_nand_inv_s(35 downto 34) <= addr_wr_to_nand_s(2 downto 1);
                addr_wr_to_nand_inv_s(20 downto 13) <= addr_wr_to_nand_s(10 downto 3);
                addr_wr_to_nand_inv_s(32 downto 21) <= addr_wr_to_nand_s(22 downto 11);
                
            end if;
        end if;
    end process;
    
    wr_to_nand: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then           
            if (iRST = '1') then 
                data_free_count_fifo2                 <= (others => '1');
                start_wr_to_nand_s                    <= '0';
                start_wr_to_nand_s1                   <= '0';
                count_pack_wr_to_nand_s                 <= (others => '0');
                wr_nand_dr_wr_to_nand_s               <= '0';
                command_data_wr_to_nand_s             <= '0';
                transmit_receive_wr_to_nand_s         <= '0';
                rd_en_fifo2_wr_to_nand_s              <= '0';
                addr_in_wr_to_nand_s                  <= (others => '0');
                data_in_wr_to_nand_s                  <= (others => '0');
                count_wr_to_nand_s                    <= (others => '0');
                
            else 
                start_wr_to_nand_s1 <= start_wr_to_nand_s;
                
                if data_count_fifo2_s((WIDTH_DATA_COUNT_FIFO23-1)downto WIDTH_DATA_COUNT_FIFO1) = conv_std_logic_vector(2**(WIDTH_DATA_COUNT_FIFO23-WIDTH_DATA_COUNT_FIFO1)-1,(WIDTH_DATA_COUNT_FIFO23-WIDTH_DATA_COUNT_FIFO1)) then
--                    data_free_count_fifo2 <= conv_std_logic_vector(2**(WIDTH_DATA_COUNT_FIFO1)-1,WIDTH_DATA_COUNT_FIFO1) - data_count_fifo2_s((WIDTH_DATA_COUNT_FIFO1-1) downto 0);
                    data_free_count_fifo2 <= not(data_count_fifo2_s((WIDTH_DATA_COUNT_FIFO1-1) downto 0));
                else
                    data_free_count_fifo2 <= (others => '1');
                end if;
                
                if ld_wr_to_eth = '1' and start_wr_to_eth_s = '0' and (data_free_count_fifo2 < count_pack_fifo1_s) and start_wr_to_nand_s = '0' then
                    start_wr_to_nand_s                       <= '1';  
                    count_pack_wr_to_nand_s                  <= data_count_fifo2_s;
                    wr_nand_dr_wr_to_nand_s                  <= '1';
                    command_data_wr_to_nand_s                <= '1';
                    transmit_receive_wr_to_nand_s            <= '1';
                    addr_in_wr_to_nand_s                     <= addr_wr_to_nand_inv_s;
                    data_in_wr_to_nand_s                     <= (others => '0');
                end if;

                if start_wr_to_nand_s = '1' and data_rdy_wr_rd_nand_dr_s = '1' then
                    if count_wr_to_nand_s < conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        count_wr_to_nand_s <= count_wr_to_nand_s + '1';
                    else
                        transmit_receive_wr_to_nand_s <= '0';
                        start_wr_to_nand_s <= '0';
                        count_wr_to_nand_s <= (others => '0');
                    end if;
                    
                    if count_wr_to_nand_s(WIDTH_DATA_COUNT_FIFO23) = '0' then
                        if count_wr_to_nand_s((WIDTH_DATA_COUNT_FIFO23-1) downto 0) < count_pack_wr_to_nand_s then
                            data_in_wr_to_nand_s <= data_out_fifo2_s;
                            rd_en_fifo2_wr_to_nand_s <= '1';
                        elsif count_wr_to_nand_s((WIDTH_DATA_COUNT_FIFO23-1) downto 0) = count_pack_wr_to_nand_s then
                            data_in_wr_to_nand_s <= data_out_fifo2_s;
                            rd_en_fifo2_wr_to_nand_s <= '0';
--                            transmit_receive_wr_to_nand_s <= '0';
                        elsif count_wr_to_nand_s((WIDTH_DATA_COUNT_FIFO23-1) downto 0) = count_pack_wr_to_nand_s + '1' then
                            data_in_wr_to_nand_s <= data_out_fifo2_s;
                        elsif count_wr_to_nand_s((WIDTH_DATA_COUNT_FIFO23-1) downto 0) > count_pack_wr_to_nand_s + '1' then
                            data_in_wr_to_nand_s <= (others => '0');
                        else
                            data_in_wr_to_nand_s <= (others => '0');
                        end if;   
                    else
                        data_in_wr_to_nand_s <= (others => '0');
                    end if;
                end if;
                
                if command_data_wr_to_nand_s = '1' then
                    command_data_wr_to_nand_s <= '0';
                end if;
                if wr_nand_dr_wr_to_nand_s = '1' then
                    wr_nand_dr_wr_to_nand_s <= '0';
                end if;               
                
            end if;
        end if;
    end process wr_to_nand;
     
    rd_from_nand: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then           
            if (iRST = '1') then
            
                ess_rd_from_nand1_s    <= (others => '0');
                ess_rd_from_nand2_s    <= (others => '0');
                ess_rd_from_nand3_s    <= (others => '0');
                ess_rd_from_nand4_s    <= (others => '0');
                ess_rd_from_nand5_s    <= (others => '0');
                ess_rd_from_nand6_s    <= (others => '0');
                ess_rd_from_nand7_s    <= (others => '0');
                ess_rd_from_nand8_s    <= (others => '0');
                ess_rd_from_nand9_s    <= (others => '0');
                ess_rd_from_nand10_s   <= (others => '0');
                ess_rd_from_nand11_s   <= (others => '0');
                ess_rd_from_nand12_s   <= (others => '0');
                ess_rd_from_nand13_s   <= (others => '0');
                ess_rd_from_nand14_s   <= (others => '0');
                ess_rd_from_nand15_s   <= (others => '0');
                ess_rd_from_nand16_s   <= (others => '0');
                
                count_rd_from_nand1_s  <= (others => '0');
                count_rd_from_nand2_s  <= (others => '0');
                count_rd_from_nand3_s  <= (others => '0');
                count_rd_from_nand4_s  <= (others => '0');
                count_rd_from_nand5_s  <= (others => '0');
                count_rd_from_nand6_s  <= (others => '0');
                count_rd_from_nand7_s  <= (others => '0');
                count_rd_from_nand8_s  <= (others => '0');
                count_rd_from_nand9_s  <= (others => '0');
                count_rd_from_nand10_s <= (others => '0');
                count_rd_from_nand11_s <= (others => '0');
                count_rd_from_nand12_s <= (others => '0');
                count_rd_from_nand13_s <= (others => '0');
                count_rd_from_nand14_s <= (others => '0');
                count_rd_from_nand15_s <= (others => '0');
                count_rd_from_nand16_s <= (others => '0');
                
                start_rd_from_nand_s   <= '0';
                start_rd_from_nand_s1  <= '0';
                count_rd_from_nand_s   <= (others => '0');
                
            else
                start_rd_from_nand_s1 <= start_rd_from_nand_s;
                
                if start_rd_from_nand_s = '1' and data_rdy_wr_rd_nand_dr_s = '1' then
                    count_rd_from_nand_s <= count_rd_from_nand_s + '1';
                    if count_rd_from_nand_s < conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        wr_en_fifo3_s <= '1';
                        data_in_fifo3_s <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        wr_en_fifo3_s   <= '0';
                        data_in_fifo3_s <= (others => '0');
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand1_s(7 downto 0) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand1_s(15 downto 8) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand1_s(23 downto 16) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand2_s(7 downto 0) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand2_s(15 downto 8) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand2_s(23 downto 16) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand3_s(7 downto 0) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand3_s(15 downto 8) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand3_s(23 downto 16) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand4_s(7 downto 0) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand4_s(15 downto 8) <= data_out_nand_dr_s;
                    elsif count_rd_from_nand_s = conv_std_logic_vector(2**WIDTH_DATA_COUNT_FIFO23+1,(WIDTH_DATA_COUNT_FIFO23+1)) then
                        ess_rd_from_nand4_s(23 downto 16) <= data_out_nand_dr_s;
                    
                    end if;
                    
                end if;
            end if;
        end if;
    end process rd_from_nand;
    
    		clk        => iCLK_50MHZ,
		srst       => iRST,
		din        => data_in_fifo3_s,
		wr_en      => wr_en_fifo3_s,
		rd_en      => rd_en_fifo3_s,
		dout       => data_out_fifo3_s,
		full       => full_fifo3_s,
		empty      => empty_fifo3_s,
		valid      => valid_fifo3_s,
		data_count => data_count_fifo3_s
    
    
    
    
rd_data_fifo1_s <= rd_data_fifo1_wr_to_eth_s or rd_data_fifo1_wr_to_fifo2_s;
rd_en_fifo2_s   <= rd_en_fifo2_wr_to_nand_s;
    
data_in_dev_s       <= iDATA_DEV;
start_pack_dev_s    <= iSTART_PACK_DEV;
wrdreq_dev_s        <= iWRDREQ_DEV; 
wrfull_eth_s        <= iWRFULL_ETH;      
wr_empt_byte_eth    <= iWR_EMPT_BYTE_ETH;

oWR_DATA_ETH        <= wr_data_eth_wr_to_eth_s;
oSTART_PACK_ETH     <= start_pack_eth_wr_to_eth_s;

rb_in_nand_dr_s        <= iRB_NAND        ;
oCLE_NAND              <= cle_nand_dr_s   ;
oCE_NAND               <= ce_nand_dr_s    ;
oWE_NAND               <= we_nand_dr_s    ;
oALE_NAND              <= ale_nand_dr_s   ;
oRE_NAND               <= re_nand_dr_s    ;
oWP_NAND               <= wp_nand_dr_s    ;
oRB_NAND               <= rb_out_nand_dr_s;
oDQS_NAND              <= '0'             ;
oDONE_RESET            <= done_reset_nand_s;
oDONE_SET_FEATURES     <= done_set_features_nand_s;


wr_nand_dr_s               <=  wr_nand_dr_wr_to_nand_s or wr_nand_dr_reset_nand_s or wr_nand_dr_set_features_nand_s; 
command_data_nand_dr_s     <= command_data_wr_to_nand_s or command_data_nand_dr_reset_nand_s or command_data_set_features_nand_s;    
set_features_in_nand_dr_s  <= set_features_in_reset_nand_s or set_features_in_set_features_nand_s;
command_nand_dr_s          <= command_reset_nand_s or command_set_features_nand_s;
transmit_receive_nand_dr_s <= transmit_receive_wr_to_nand_s;
addr_in_nand_dr_s          <= addr_in_wr_to_nand_s or addr_in_set_features_nand_s;
data_in_nand_dr_s          <= data_in_wr_to_nand_s or data_in_set_features_nand_s;

end Behavioral;

