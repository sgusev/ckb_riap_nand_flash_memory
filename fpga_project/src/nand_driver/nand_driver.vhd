--------------------------------------------------------------------------------
-- Entity: nand_driver
-- Date:2013-05-23  
-- Author: Sergey Gusev     
--
-- Description ${cursor}
--------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
    use IEEE.STD_LOGIC_ARITH.ALL;    
    
entity nand_driver is
    generic( 
            WIDTH_ADDR_NAND_DR  : integer := 36; --width addr bus
            WIDTH_DATA_NAND_DR  : integer := 8;   --width data bus
            WIDTH_COMMAND       : integer := 8;   --width command bus
            WIDTH_CE            : integer := 8   --width CE bus
            );
    port(
        --IN--
        iCLK_50MHZ        : in std_logic;                                                 -- input clock, 50 MHz.
        iCLK_100MHZ        : in std_logic;                                                 -- input clock, 100 MHz.
        iRST              : in std_logic;                                                 -- reset
        iCOMMAND_DATA     : in std_logic;                                                 -- iCOMMAND_DATA = 0 -transmit/receive command; iCOMMAND_DATA = 1 -transmit/receive data
        iSET_FEATURES     : in std_logic;                                                 -- iSET_FEATURES = 1 - set features
        iCOMMAND          : in std_logic_vector((WIDTH_COMMAND-1) downto 0);
        iRD               : in std_logic;                                                 -- iRD = 1 - reade; 
        iWR               : in std_logic;                                                 -- iWR = 1 - write;
        iTRANSMIT_RECEIVE : in std_logic;                                                 -- iWR = 1 - write;
        iRB               : in std_logic;
        iADDR             : in std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0);
        iDATA             : in std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);
        iDQ               : in std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);
        --OUT--
        oDATA               : out std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);
        oDATA_RDY_WR_RD     : out std_logic;                                            -- oDATA_RDY_WR_RD = 1 - data ready for read/write;
        oLD                 : out std_logic;                                            -- oLD = 1 - load command
        oTRANSMIT_COMMAND   : out std_logic;                                            -- oTRANSMIT_COMMAND = 1 -transmit command
        oSET_FEATURES       : out std_logic;                                            -- oSET_FEATURES = 1 - set features
        oCLE                : out std_logic;                                            -- NAND signal
        oCE                 : out std_logic_vector((WIDTH_CE-1) downto 0);                         -- NAND signal
        oWE                 : out std_logic;                                            -- NAND signal
        oALE                : out std_logic;                                            -- NAND signal
        oRE                 : out std_logic;                                            -- NAND signal
        oWP                 : out std_logic;                                            -- NAND signal
        oDQ                 : out std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);  -- NAND signal
        oDQ_T               : out std_logic;                                            -- oDQ_T = 0 - oDQ-output; oDQ_T = 1 - oDQ-input       
        oRB                 : out std_logic
        );
end nand_driver;

architecture arch of nand_driver is

constant SET_FTR_COUNT_C    : integer := 7;
constant TRNSM_COM_COUNT_C  : integer := 4;
constant WR_DATA_COUNT_C    : integer := 8;
constant RD_DATA_COUNT_C    : integer := 5;
constant WE_WR_DATA_COUNT_C : integer := 1;
constant RE_RD_DATA_COUNT_C : integer := 1;
constant WR_DATA_ECC_COUNT_C : integer := 14;

signal  ld_s                                : std_logic := '0';
signal  ld_tr_cmd_mode1_s                   : std_logic := '0';
signal  ld_st_ftrs_md1_s                    : std_logic := '0';
signal  ld_strt_wr_dt_s                     : std_logic := '0';
signal  ld_strt_rd_dt_s                     : std_logic := '0';
signal  transmit_command_mode1_s          : std_logic := '0';
signal  set_features_mode1_s              : std_logic := '0';
signal  set_features_done_s               : std_logic := '0';
signal  set_features_done_s1              : std_logic := '0';
signal  set_features_done_s2              : std_logic := '0';
signal  set_features_done_s3              : std_logic := '0';
signal  set_features_done_s4              : std_logic := '0';
signal  set_features_done_s5              : std_logic := '0';
signal  set_features_done_s6              : std_logic := '0';
signal  start_wr_data_s                   : std_logic := '0';
signal  start_wr_data_s1                  : std_logic := '0';
signal  start_wr_data_s2                  : std_logic := '0';
signal  start_wr_data_s3                  : std_logic := '0';
signal  start_wr_data_s4                  : std_logic := '0';
signal  start_rd_data_s                   : std_logic := '0';
signal  start_rd_data_s1                  : std_logic := '0';
signal  start_rd_data_s2                  : std_logic := '0';
signal  start_rd_data_s3                  : std_logic := '0';
signal  start_rd_data_s4                  : std_logic := '0';



signal  command_s                         : std_logic_vector((WIDTH_COMMAND-1) downto 0) := (others => '0');
signal  command_tr_cmd_mode1_s            : std_logic_vector((WIDTH_COMMAND-1) downto 0) := (others => '0');
signal  command_st_ftrs_md1_s            : std_logic_vector((WIDTH_COMMAND-1) downto 0) := (others => '0');
signal  addr_s                            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_st_ftrs_md1_s                            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_strt_wr_dt_s                            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_strt_wr_dt_s1                            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_strt_wr_dt_s2                            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_strt_wr_dt_s3                            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_strt_rd_dt_s                            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_strt_rd_dt_s1                            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_strt_rd_dt_s2                            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_strt_rd_dt_s3                            : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');

signal  addr_s1                           : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_s2                           : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  addr_s3                           : std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0) := (others => '0');
signal  data_in_s                         : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  trnsm_com_count_mode1_s           : std_logic_vector((TRNSM_COM_COUNT_C-1) downto 0) := (others => '0');
signal  set_ftr_count_mode1_s                   : std_logic_vector((SET_FTR_COUNT_C-1) downto 0) := (others => '0');
signal  wr_data_count_s                   : std_logic_vector((WR_DATA_COUNT_C-1) downto 0) := (others => '0');
signal  rd_data_count_s                   : std_logic_vector((RD_DATA_COUNT_C-1) downto 0) := (others => '0');
signal  we_wr_data_count_s                   : std_logic_vector((WE_WR_DATA_COUNT_C-1) downto 0) := (others => '0');
signal  re_rd_data_count_s                   : std_logic_vector((RE_RD_DATA_COUNT_C-1) downto 0) := (others => '0');

signal  cle_s                             : std_logic := '0';
signal  ce_s                              : std_logic_vector(7 downto 0) := (others => '1');
signal  we_s                              : std_logic := '1';
signal  ale_s                             : std_logic := '0';
signal  re_s                              : std_logic := '1';
signal  wp_s                              : std_logic := '1';
signal  rb_s                              : std_logic := '0';
signal  rb_s1                             : std_logic := '0';
signal  rb_s2                             : std_logic := '0';
signal  rb_s3                             : std_logic := '0';
signal  dq_out_s                          : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_t_s                            : std_logic := '0';

signal  cle_tr_cmd_mode1_s                : std_logic := '0';
signal  ce_tr_cmd_mode1_s                 : std_logic_vector(7 downto 0) := (others => '1');
signal  we_tr_cmd_mode1_s                 : std_logic := '1';
signal  ale_tr_cmd_mode1_s                : std_logic := '0';
signal  re_tr_cmd_mode1_s                 : std_logic := '1';
signal  wp_tr_cmd_mode1_s                 : std_logic := '1';
signal  dq_out_tr_cmd_mode1_s             : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_t_tr_cmd_mode1_s               : std_logic := '0';

signal  cle_st_ftrs_md1_s                : std_logic := '0';
signal  ce_st_ftrs_md1_s                 : std_logic_vector(7 downto 0) := (others => '1');
signal  we_st_ftrs_md1_s                 : std_logic := '1';
signal  ale_st_ftrs_md1_s                : std_logic := '0';
signal  re_st_ftrs_md1_s                 : std_logic := '1';
signal  wp_st_ftrs_md1_s                 : std_logic := '1';
signal  dq_out_st_ftrs_md1_s             : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_t_st_ftrs_md1_s               : std_logic := '0';

signal  cle_wr_dt_s                         : std_logic := '0';
signal  ce_wr_dt_s                          : std_logic_vector(7 downto 0) := (others => '1');
signal  we_wr_dt_s                          : std_logic := '1';
signal  ale_wr_dt_s                         : std_logic := '0';
signal  re_wr_dt_s                          : std_logic := '1';
signal  wp_wr_dt_s                          : std_logic := '1';
signal  dq_out_wr_dt_s                      : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_t_wr_dt_s                        : std_logic := '0';

signal  cle_rd_dt_s                         : std_logic := '0';
signal  ce_rd_dt_s                          : std_logic_vector(7 downto 0) := (others => '1');
signal  we_rd_dt_s                          : std_logic := '1';
signal  ale_rd_dt_s                         : std_logic := '0';
signal  re_rd_dt_s                          : std_logic := '1';
signal  wp_rd_dt_s                          : std_logic := '1';
signal  dq_out_rd_dt_s                      : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_in_rd_dt_s                      : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_in_rd_dt_s1                      : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_in_rd_dt_s2                      : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_t_rd_dt_s                        : std_logic := '0';

signal  cle_s1                            : std_logic := '0';
signal  ce_s1                             : std_logic_vector(7 downto 0) := (others => '1');
signal  we_s1                             : std_logic := '1';
signal  ale_s1                            : std_logic := '0';
signal  re_s1                             : std_logic := '1';
signal  dq_out_s1                         : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_in_s                           : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  data_in_st_ftrs_md1_s             : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  data_in_strt_wr_dt_s1               : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  data_in_strt_wr_dt_s2               : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_in_s1                          : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_in_s2                          : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
signal  dq_t_s1                           : std_logic := '0';
signal  wr_ecc_count_s                      : std_logic := '0';
signal  data_rdy_wr_s                  : std_logic := '0';
signal  data_rdy_wr_s1                 : std_logic := '0';
signal  data_rdy_wr_s2                 : std_logic := '0';
signal  data_rdy_rd_s                  : std_logic := '0';
signal  data_rdy_rd_s1                 : std_logic := '0';
signal  data_rdy_rd_s2                 : std_logic := '0';
signal  strt_we_wr_data_count_s           : std_logic := '0';
signal  strt_we_rd_data_count_s           : std_logic := '0';
signal  strt_re_rd_data_count_s           : std_logic := '0';

signal  start_ecc_wr_data_s            : std_logic := '0';
signal  ecc_wr_data_s                  : std_logic := '0';
signal  start_ecc_wr_data_s1           : std_logic := '0';
signal  start_ecc_wr_data_s2           : std_logic := '0';
signal  count_wr_data_ecc_wr    :std_logic_vector((WR_DATA_ECC_COUNT_C-1) downto 0) := (others => '0');
signal  p1_s1                   : std_logic := '0';
signal  p2_s1                   : std_logic := '0';
signal  p4_s1                   : std_logic := '0';
signal  p8_s1                   : std_logic := '0';
signal  p16_s1                  : std_logic := '0';
signal  p32_s1                  : std_logic := '0';
signal  p64_s1                  : std_logic := '0';
signal  p128_s1                 : std_logic := '0';
signal  p256_s1                 : std_logic := '0';
signal  p512_s1                 : std_logic := '0';
signal  p1024_s1                : std_logic := '0';
signal  p2048_s1                : std_logic := '0';
signal  p1_s2                   : std_logic := '0';
signal  p2_s2                   : std_logic := '0';
signal  p4_s2                   : std_logic := '0';
signal  p8_s2                   : std_logic := '0';
signal  p16_s2                  : std_logic := '0';
signal  p32_s2                  : std_logic := '0';
signal  p64_s2                  : std_logic := '0';
signal  p128_s2                 : std_logic := '0';
signal  p256_s2                 : std_logic := '0';
signal  p512_s2                 : std_logic := '0';
signal  p1024_s2                : std_logic := '0';
signal  p2048_s2                : std_logic := '0';
signal p8_eq_s                  : std_logic := '0';
signal p16_eq_s                 : std_logic := '0';
signal p32_eq_s                 : std_logic := '0';
signal p64_eq_s                 : std_logic := '0';
signal p128_eq_s                : std_logic := '0';
signal p256_eq_s                : std_logic := '0';
signal p512_eq_s                : std_logic := '0';
signal p1024_eq_s                : std_logic := '0';
signal p2048_eq_s                : std_logic := '0';
signal p8_strb_s                : std_logic := '0';
signal p16_strb_s               : std_logic := '0';
signal p32_strb_s               : std_logic := '0';
signal p64_strb_s               : std_logic := '0';
signal p128_strb_s              : std_logic := '0';
signal p256_strb_s              : std_logic := '0';
signal p512_strb_s              : std_logic := '0';
signal p1024_strb_s              : std_logic := '0';
signal p2048_strb_s              : std_logic := '0';
signal ess_wr_word1_s              : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word2_s              : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word3_s              : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word4_s              : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word5_s              : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word6_s              : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word7_s              : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word8_s              : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word9_s              : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word10_s             : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word11_s             : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word12_s             : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word13_s             : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word14_s             : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word15_s             : std_logic_vector(23 downto 0) := (others => '0');
signal ess_wr_word16_s             : std_logic_vector(23 downto 0) := (others => '0');

signal start_count_lngt_pckg_s      : std_logic := '0';
signal start_count_lngt_pckg_s1     : std_logic := '0';
signal start_count_lngt_pckg_s2     : std_logic := '0';
signal count_lngt_one_pckg_s        : std_logic := '0';
signal count_lngt_one_pckg_s1       : std_logic := '0';
signal count_lngt_pckg_s            : std_logic := '0';
signal lngt_one_pckg_s              : std_logic_vector(15 downto 0) := (others => '0');
signal number_count_lngt_one_pckg_s : std_logic_vector(3 downto 0) := (others => '0');
signal count_lngt_pckg1_s           : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg2_s           : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg3_s           : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg4_s           : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg5_s           : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg6_s           : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg7_s           : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg8_s           : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg9_s           : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg10_s          : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg11_s          : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg12_s          : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg13_s          : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg14_s          : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg15_s          : std_logic_vector(15 downto 0) := (others => '0');
signal count_lngt_pckg16_s          : std_logic_vector(15 downto 0) := (others => '0');

begin

    process_rb: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then
            rb_s <= iRB;
            rb_s1 <= rb_s;        
        end if;
    end process process_rb;
    
    process_transmit_command_mode1: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then
            if (iRST = '1') then
                transmit_command_mode1_s    <= '0';            
                ld_tr_cmd_mode1_s           <= '0'; 
                command_tr_cmd_mode1_s      <= (others => '0');   
                cle_tr_cmd_mode1_s          <= '0';            
                ce_tr_cmd_mode1_s           <= (others => '1');           
                we_tr_cmd_mode1_s           <= '1';      
                ale_tr_cmd_mode1_s          <= '0';         
                re_tr_cmd_mode1_s           <= '1';        
                wp_tr_cmd_mode1_s           <= '1';       
                dq_out_tr_cmd_mode1_s       <= (others => '0');              
                dq_t_tr_cmd_mode1_s         <= '0'; 
                trnsm_com_count_mode1_s     <= (others => '0');
            else
                if ld_tr_cmd_mode1_s = '1' then
                    ld_tr_cmd_mode1_s <= '0';
                end if;
                
                if transmit_command_mode1_s = '0' and set_features_mode1_s = '0' then
                    if (iWR = '1' and iCOMMAND_DATA = '0' and iSET_FEATURES = '0') then
                        ld_tr_cmd_mode1_s        <= '1';
                        command_tr_cmd_mode1_s   <= iCOMMAND;
                        transmit_command_mode1_s <= '1';
                    end if;
                end if;
                
                if transmit_command_mode1_s = '1' then
                    if (trnsm_com_count_mode1_s < conv_std_logic_vector(4,TRNSM_COM_COUNT_C)) then
                        cle_tr_cmd_mode1_s               <= '1';
                        ce_tr_cmd_mode1_s                <= (others => '0');
                        we_tr_cmd_mode1_s                <= '0';
                        dq_out_tr_cmd_mode1_s           <= command_tr_cmd_mode1_s;
                        dq_t_tr_cmd_mode1_s             <= '0';
                        trnsm_com_count_mode1_s  <= trnsm_com_count_mode1_s + '1';
                    elsif (trnsm_com_count_mode1_s < conv_std_logic_vector(7,TRNSM_COM_COUNT_C)) then
                        we_tr_cmd_mode1_s <= '1';
                        trnsm_com_count_mode1_s  <= trnsm_com_count_mode1_s + '1';
                    elsif (trnsm_com_count_mode1_s = conv_std_logic_vector(7,TRNSM_COM_COUNT_C)) then
                        cle_tr_cmd_mode1_s          <= '0';
                        ce_tr_cmd_mode1_s           <= (others => '1');
                        dq_out_tr_cmd_mode1_s       <= (others => '0'); 
                        dq_t_tr_cmd_mode1_s         <= '0';
                        trnsm_com_count_mode1_s     <= (others => '0');
                        transmit_command_mode1_s    <= '0';                 
                    end if;
                end if;
            end if;
        end if;
    end process process_transmit_command_mode1;
    
    process_set_features_mode1: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then
            if (iRST = '1') then  
                ld_st_ftrs_md1_s <= '0';
                set_features_mode1_s <= '0';
                command_st_ftrs_md1_s <= (others => '0');
                addr_st_ftrs_md1_s <= (others => '0');
                data_in_st_ftrs_md1_s <= (others => '0');
                set_ftr_count_mode1_s     <= (others => '0');
                
                cle_st_ftrs_md1_s          <= '0';            
                ce_st_ftrs_md1_s           <= (others => '1');           
                we_st_ftrs_md1_s           <= '1';      
                ale_st_ftrs_md1_s          <= '0';         
                re_st_ftrs_md1_s           <= '1';        
                wp_st_ftrs_md1_s           <= '1';       
                dq_out_st_ftrs_md1_s       <= (others => '0');              
                dq_t_st_ftrs_md1_s         <= '0';               
               
            else
                if ld_st_ftrs_md1_s = '1' then
                    ld_st_ftrs_md1_s <= '0';
                end if;
                
                if transmit_command_mode1_s = '0' and set_features_mode1_s = '0' then                  
                    if (iWR = '1' and iCOMMAND_DATA = '0' and iSET_FEATURES = '1') then
                        ld_st_ftrs_md1_s        <= '1';
                        command_st_ftrs_md1_s   <= iCOMMAND;
                        addr_st_ftrs_md1_s      <= iADDR;
                        data_in_st_ftrs_md1_s   <= iDATA;
                        set_features_mode1_s <= '1';
                    end if;
                end if;
                
                if  set_features_mode1_s = '1' then 
                    if (set_ftr_count_mode1_s < conv_std_logic_vector(5,SET_FTR_COUNT_C)) then
                        cle_st_ftrs_md1_s           <= '1';
                        ce_st_ftrs_md1_s            <= (others => '0');
                        we_st_ftrs_md1_s            <= '0';
                        ale_st_ftrs_md1_s           <= '0';
                        re_st_ftrs_md1_s            <= '1';
                        wp_st_ftrs_md1_s            <= '1';
                        dq_out_st_ftrs_md1_s        <= command_st_ftrs_md1_s;
                        dq_t_st_ftrs_md1_s          <= '0';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';
                    elsif (set_ftr_count_mode1_s = conv_std_logic_vector(5,SET_FTR_COUNT_C)) then
                        we_st_ftrs_md1_s            <= '1';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';
                    elsif (set_ftr_count_mode1_s = conv_std_logic_vector(6,SET_FTR_COUNT_C)) then
                        cle_st_ftrs_md1_s           <= '0';
                        ce_st_ftrs_md1_s            <= (others => '1');
                        ale_st_ftrs_md1_s           <= '1';
                        dq_out_st_ftrs_md1_s        <= (others => '0');
                        dq_t_st_ftrs_md1_s          <= '0';                        
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';  
                    elsif (set_ftr_count_mode1_s = conv_std_logic_vector(8,SET_FTR_COUNT_C)) then
                        ce_st_ftrs_md1_s            <= (others => '0');
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(12,SET_FTR_COUNT_C)) then
                        we_st_ftrs_md1_s            <= '0';
                        dq_out_st_ftrs_md1_s        <= addr_st_ftrs_md1_s(7 downto 0);
                        dq_t_st_ftrs_md1_s          <= '0';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(15,SET_FTR_COUNT_C)) then  
                        we_st_ftrs_md1_s            <= '1';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1'; 
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(61-30,SET_FTR_COUNT_C)) then  
                        ale_st_ftrs_md1_s           <= '0'; 
                        dq_out_st_ftrs_md1_s        <= (others => '0');
                        dq_t_st_ftrs_md1_s          <= '0';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1'; 
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(63+1-30,SET_FTR_COUNT_C)) then  
                        we_st_ftrs_md1_s            <= '0';
                        dq_out_st_ftrs_md1_s        <= data_in_st_ftrs_md1_s;
                        dq_t_st_ftrs_md1_s          <= '0';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(65+2-30,SET_FTR_COUNT_C)) then 
                        we_st_ftrs_md1_s            <= '1';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';     
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(67+3-30,SET_FTR_COUNT_C)) then  
                        we_st_ftrs_md1_s            <= '0';
                        dq_out_st_ftrs_md1_s        <= (others => '0');
                        dq_t_st_ftrs_md1_s          <= '0';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(69+4-30,SET_FTR_COUNT_C)) then 
                        we_st_ftrs_md1_s            <= '1';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';        
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(71+5-30,SET_FTR_COUNT_C)) then  
                        we_st_ftrs_md1_s            <= '0';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(73+6-30,SET_FTR_COUNT_C)) then 
                        we_st_ftrs_md1_s            <= '1';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';  
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(75+7-30,SET_FTR_COUNT_C)) then  
                        we_st_ftrs_md1_s            <= '0';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(77+8-30,SET_FTR_COUNT_C)) then 
                        we_st_ftrs_md1_s            <= '1';
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';  
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(79+9-30,SET_FTR_COUNT_C)) then  
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1';
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(81+10-30,SET_FTR_COUNT_C)) then 
                        we_st_ftrs_md1_s            <= '1';
                        ce_st_ftrs_md1_s            <= (others => '1');
                        set_ftr_count_mode1_s <= set_ftr_count_mode1_s + '1'; 
                    elsif (set_ftr_count_mode1_s < conv_std_logic_vector(82+10-30,SET_FTR_COUNT_C)) then 
                        cle_st_ftrs_md1_s          <= '0';            
                        ce_st_ftrs_md1_s           <= (others => '1');           
                        we_st_ftrs_md1_s           <= '1';      
                        ale_st_ftrs_md1_s          <= '0';         
                        re_st_ftrs_md1_s           <= '1';        
                        wp_st_ftrs_md1_s           <= '1';       
                        dq_out_st_ftrs_md1_s       <= (others => '0');              
                        dq_t_st_ftrs_md1_s         <= '0';  
                        set_ftr_count_mode1_s      <= (others => '0');                         
                        set_features_mode1_s       <= '0';
                        set_features_done_s        <= '1';
                    end if;                                    
                end if;
            end if;
        end if;
    end process process_set_features_mode1;
    
    start_write_data: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then          
            if (iRST = '1') then
                ld_strt_wr_dt_s     <= '0';
                addr_strt_wr_dt_s   <= (others => '0');
                start_wr_data_s     <= '0';
            else
                if (ld_strt_wr_dt_s = '1') then
                    ld_strt_wr_dt_s <= '0';
                end if;
                
                if (iWR = '1' and iCOMMAND_DATA = '1' and iSET_FEATURES = '0' and iTRANSMIT_RECEIVE = '1') then
                    ld_strt_wr_dt_s         <= '1';
                    addr_strt_wr_dt_s       <= iADDR;
                    start_wr_data_s         <= '1';
                elsif (iTRANSMIT_RECEIVE = '0') then
                    start_wr_data_s <= '0';
                end if;            
            end if;
        end if;
    end process start_write_data;
    
    start_read_data: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then          
            if (iRST = '1') then
                ld_strt_rd_dt_s     <= '0';
                addr_strt_rd_dt_s   <= (others => '0');
                start_rd_data_s     <= '0';
            else
                if (ld_strt_rd_dt_s = '1') then
                    ld_strt_rd_dt_s <= '0';
                end if;
                
                if (iRD = '1' and iCOMMAND_DATA = '1' and iSET_FEATURES = '0' and iTRANSMIT_RECEIVE = '1') then
                    ld_strt_rd_dt_s         <= '1';
                    addr_strt_rd_dt_s       <= iADDR;
                    start_rd_data_s         <= '1';
                elsif (iTRANSMIT_RECEIVE = '0') then
                    start_rd_data_s <= '0';
                end if;            
            end if;
        end if;
    end process start_read_data;
     
    we_wr_data: process(iCLK_100MHZ)
    begin
        if(iCLK_100MHZ'event and iCLK_100MHZ = '1') then
            if (iRST = '1') then
                we_wr_data_count_s <= (others => '1');
            else
                if strt_we_wr_data_count_s = '1' or strt_we_rd_data_count_s = '1' then
                    we_wr_data_count_s <= we_wr_data_count_s + '1';
                else
                    we_wr_data_count_s <= (others => '1');
                end if;
            end if;
        end if;
    end process we_wr_data;
    we_wr_dt_s <= we_wr_data_count_s(WE_WR_DATA_COUNT_C-1);
    
    re_rd_data: process(iCLK_100MHZ)
    begin
        if(iCLK_100MHZ'event and iCLK_100MHZ = '1') then
            if (iRST = '1') then
                re_rd_data_count_s <= (others => '1');
            else
                if strt_re_rd_data_count_s = '1' then
                    re_rd_data_count_s <= re_rd_data_count_s + '1';
                else
                    re_rd_data_count_s <= (others => '1');
                end if;
            end if;
        end if;
    end process re_rd_data;
    re_rd_dt_s <= re_rd_data_count_s(RE_RD_DATA_COUNT_C-1);
    
    from_50MHZ_to_100MHZ: process(iCLK_100MHZ)
    begin
        if(iCLK_100MHZ'event and iCLK_100MHZ = '1') then
            start_wr_data_s1    <= start_wr_data_s;
            start_wr_data_s2    <= start_wr_data_s1;
            start_wr_data_s3    <= start_wr_data_s2;
            start_wr_data_s4    <= start_wr_data_s3;
            addr_strt_wr_dt_s1  <= addr_strt_wr_dt_s;
            addr_strt_wr_dt_s2  <= addr_strt_wr_dt_s1;
            start_rd_data_s1    <= start_rd_data_s;
            start_rd_data_s2    <= start_rd_data_s1;
            start_rd_data_s3    <= start_rd_data_s2;
            start_rd_data_s4    <= start_rd_data_s3;
            addr_strt_rd_dt_s1  <= addr_strt_rd_dt_s;
            addr_strt_rd_dt_s2  <= addr_strt_rd_dt_s1;
            data_in_strt_wr_dt_s1 <= data_in_s;
            data_in_strt_wr_dt_s2 <= data_in_strt_wr_dt_s1;  

            rb_s2 <= rb_s1;
            rb_s3 <= rb_s2;
            
        end if;
    end process from_50MHZ_to_100MHZ;
    
    from_100MHZ_to_50MHZ: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then
            data_rdy_wr_s1 <= data_rdy_wr_s;        
            data_rdy_wr_s2 <= data_rdy_wr_s1;
            data_rdy_rd_s1 <= data_rdy_rd_s;        
            data_rdy_rd_s2 <= data_rdy_rd_s1;
            dq_in_rd_dt_s1 <= dq_in_rd_dt_s;
            dq_in_rd_dt_s2 <= dq_in_rd_dt_s1;
        end if;
    end process from_100MHZ_to_50MHZ;    
    
    
    write_data: process(iCLK_100MHZ)
    begin
        if(iCLK_100MHZ'event and iCLK_100MHZ = '1') then           
            if (iRST = '1') then
                wr_data_count_s <= conv_std_logic_vector(0,WR_DATA_COUNT_C);
                addr_strt_wr_dt_s3    <= (others => '0');
                strt_we_wr_data_count_s <= '0';
                
                cle_wr_dt_s          <= '0';            
                ce_wr_dt_s           <= (others => '1');              
                ale_wr_dt_s          <= '0';         
                re_wr_dt_s           <= '1';        
                wp_wr_dt_s           <= '1';       
                dq_out_wr_dt_s       <= (others => '0');              
                dq_t_wr_dt_s         <= '0';  
                
                data_rdy_wr_s    <= '0';   
                wr_ecc_count_s     <= '0';
                
            else
                if start_wr_data_s2 = '1' then
                    if wr_data_count_s = conv_std_logic_vector(0,WR_DATA_COUNT_C) then
                        wr_data_count_s <= conv_std_logic_vector(1,WR_DATA_COUNT_C);
                        addr_strt_wr_dt_s3 <= addr_strt_wr_dt_s2;
                    elsif wr_data_count_s < conv_std_logic_vector(4,WR_DATA_COUNT_C) then
                        if addr_strt_wr_dt_s3(35 downto 33) = "000" then
                            ce_wr_dt_s <= "11111110";
                        elsif addr_strt_wr_dt_s3(35 downto 33) = "001" then
                            ce_wr_dt_s <= "11111101";                   
                        elsif addr_strt_wr_dt_s3(35 downto 33) = "010" then
                            ce_wr_dt_s <= "11111011";                    
                        elsif addr_strt_wr_dt_s3(35 downto 33) = "011" then
                            ce_wr_dt_s <= "11110111";
                        elsif addr_strt_wr_dt_s3(35 downto 33) = "100" then
                            ce_wr_dt_s <= "11101111";
                        elsif addr_strt_wr_dt_s3(35 downto 33) = "101" then
                            ce_wr_dt_s <= "11011111";
                        elsif addr_strt_wr_dt_s3(35 downto 33) = "110" then
                            ce_wr_dt_s <= "10111111";
                        elsif addr_strt_wr_dt_s3(35 downto 33) = "111" then
                            ce_wr_dt_s <= "01111111";
                        end if;
                        strt_we_wr_data_count_s <= '1';
                        cle_wr_dt_s <= '1';
                        ale_wr_dt_s <= '0';
                        dq_out_wr_dt_s <= X"80";
                        dq_t_wr_dt_s <= '0';
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s < conv_std_logic_vector(6,WR_DATA_COUNT_C) then
                        cle_wr_dt_s <= '0';
                        ale_wr_dt_s <= '1';
                        dq_out_wr_dt_s <= addr_strt_wr_dt_s3(7 downto 0);
                        dq_t_wr_dt_s <= '0';
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s < conv_std_logic_vector(10-2,WR_DATA_COUNT_C) then
                        dq_out_wr_dt_s <= "000" & addr_strt_wr_dt_s3(12 downto 8);
                        dq_t_wr_dt_s <= '0';
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s < conv_std_logic_vector(12-2,WR_DATA_COUNT_C) then
                        dq_out_wr_dt_s <= addr_strt_wr_dt_s3(20 downto 13);
                        dq_t_wr_dt_s <= '0';
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s < conv_std_logic_vector(12,WR_DATA_COUNT_C) then
                        dq_out_wr_dt_s <= addr_strt_wr_dt_s3(28 downto 21);
                        dq_t_wr_dt_s <= '0';
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s = conv_std_logic_vector(12,WR_DATA_COUNT_C) then
                        dq_out_wr_dt_s <= "0000" & addr_strt_wr_dt_s3(32 downto 29);
                        dq_t_wr_dt_s <= '0';
                        wr_data_count_s <= wr_data_count_s + '1';                    
                    elsif wr_data_count_s = conv_std_logic_vector(13,WR_DATA_COUNT_C) then
                        strt_we_wr_data_count_s <= '0';
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s = conv_std_logic_vector(14,WR_DATA_COUNT_C) then
--                        strt_we_wr_data_count_s <= '0';
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s < conv_std_logic_vector(18-2,WR_DATA_COUNT_C) then
                        ale_wr_dt_s <= '0';
                        dq_out_wr_dt_s <= (others => '0');
                        dq_t_wr_dt_s <= '0';
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s < conv_std_logic_vector(18,WR_DATA_COUNT_C) then
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s = conv_std_logic_vector(18,WR_DATA_COUNT_C) then
                        data_rdy_wr_s <= '1'; --write in other plase?
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s < conv_std_logic_vector(25+4,WR_DATA_COUNT_C) then --3_07
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s = conv_std_logic_vector(25+4,WR_DATA_COUNT_C) then --3_07
                        strt_we_wr_data_count_s <= '1';
                        wr_data_count_s <= wr_data_count_s + '1';
                    elsif wr_data_count_s = conv_std_logic_vector(26+4,WR_DATA_COUNT_C) then --3_07
                        dq_out_wr_dt_s <= data_in_strt_wr_dt_s2;
                        dq_t_wr_dt_s <= '0';
                    end if;
                else
                    
                    if start_wr_data_s3 = '0' AND start_wr_data_s4 = '0' then
                        data_rdy_wr_s        <= '0';
                    end if;
                    if start_wr_data_s3 = '1' OR start_wr_data_s4 = '1' then
                        wr_ecc_count_s <= '1';
                        strt_we_wr_data_count_s <= '0';
                    end if; 
                                       
                    
                    if wr_ecc_count_s = '1' then
                        if wr_data_count_s = conv_std_logic_vector(30,WR_DATA_COUNT_C) then
                            if ecc_wr_data_s = '0' then
                                wr_data_count_s <= wr_data_count_s + '1';
                                strt_we_wr_data_count_s <= '1';
                            else
                                strt_we_wr_data_count_s <= '0';
                            end if;
                        elsif wr_data_count_s < conv_std_logic_vector(33,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word1_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(35,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word1_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(37,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word1_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(39,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word2_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(41,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word2_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(43,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word2_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(45,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word3_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(47,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word3_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(49,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word3_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(51,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word4_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(53,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word4_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(55,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word4_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(57,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word5_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(59,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word5_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(61,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word5_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                        
                        elsif wr_data_count_s < conv_std_logic_vector(63,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word6_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(65,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word6_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(67,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word6_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                            
                        elsif wr_data_count_s < conv_std_logic_vector(69,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word7_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(71,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word7_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(73,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word7_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                            
                        elsif wr_data_count_s < conv_std_logic_vector(75,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word8_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(77,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word8_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(79,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word8_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                            
                        elsif wr_data_count_s < conv_std_logic_vector(81,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word9_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(83,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word9_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(85,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word9_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                            
                        elsif wr_data_count_s < conv_std_logic_vector(87,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word10_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(89,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word10_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(91,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word10_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                            
                        elsif wr_data_count_s < conv_std_logic_vector(93,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word11_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(95,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word11_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(97,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word11_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                            
                        elsif wr_data_count_s < conv_std_logic_vector(99,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word12_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(101,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word12_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(103,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word12_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(105,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word13_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(107,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word13_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(109,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word13_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(111,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word14_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(113,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word14_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(115,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word14_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(117,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word15_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(119,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word15_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(121,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word15_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(123,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word16_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(125,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word16_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s < conv_std_logic_vector(127,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s <= ess_wr_word16_s(23 downto 16);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(129,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg1_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(131,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg1_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                        
                        elsif wr_data_count_s < conv_std_logic_vector(133,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg2_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(135,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg2_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(137,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg3_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(139,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg3_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(141,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg4_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(143,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg4_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(145,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg5_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(147,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg5_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(149,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg6_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(151,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg6_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(153,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg7_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(155,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg7_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(157,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg8_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(159,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg8_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(161,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg9_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(163,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg9_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(165,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg10_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(167,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg10_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(169,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg11_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(171,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg11_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(173,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg12_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(175,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg12_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(177,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg13_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(179,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg13_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(181,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg14_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(183,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg14_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(185,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg15_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(187,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg15_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                             
                        elsif wr_data_count_s < conv_std_logic_vector(189,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg16_s(7 downto 0);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                         
                         elsif wr_data_count_s < conv_std_logic_vector(191,WR_DATA_COUNT_C) then          
                            dq_out_wr_dt_s <= count_lngt_pckg16_s(15 downto 8);
                            dq_t_wr_dt_s <= '0';
                            wr_data_count_s <= wr_data_count_s + '1';                          
                        elsif wr_data_count_s < conv_std_logic_vector(193,WR_DATA_COUNT_C) then
                            dq_out_wr_dt_s  <= X"10";
                            dq_t_wr_dt_s    <= '0';
                            cle_wr_dt_s     <= '1';
                            wr_data_count_s <= wr_data_count_s + '1';
                        elsif wr_data_count_s = conv_std_logic_vector(193,WR_DATA_COUNT_C) then
                            cle_wr_dt_s             <= '0';            
                            ce_wr_dt_s              <= (others => '1');              
                            ale_wr_dt_s             <= '0';         
                            re_wr_dt_s              <= '1';        
                            wp_wr_dt_s              <= '1';       
                            dq_out_wr_dt_s          <= (others => '0');              
                            dq_t_wr_dt_s            <= '0'; 
                            wr_data_count_s         <= (others => '0');
                            strt_we_wr_data_count_s <= '0';
                            wr_ecc_count_s <= '0';                            
                        end if;
                    end if;
                    
                    -- if start_wr_data_s3 = '1' OR start_wr_data_s4 = '1' then
                        -- dq_out_wr_dt_s  <= X"10";
                        -- dq_t_wr_dt_s    <= '0';
                        -- cle_wr_dt_s     <= '1';
                    -- else
                        -- cle_wr_dt_s             <= '0';            
                        -- ce_wr_dt_s              <= (others => '1');              
                        -- ale_wr_dt_s             <= '0';         
                        -- re_wr_dt_s              <= '1';        
                        -- wp_wr_dt_s              <= '1';       
                        -- dq_out_wr_dt_s          <= (others => '0');              
                        -- dq_t_wr_dt_s            <= '0'; 
                        -- data_rdy_wr_s        <= '0';
                        -- wr_data_count_s         <= (others => '0');
                        -- strt_we_wr_data_count_s <= '0';                    
                    -- end if; 
                end if;
            end if;
        end if;
    end process write_data;
    
    ecc_wr_data: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then           
            if (iRST = '1') then  
                p1_s1                <= '0';
                p2_s1                <= '0';
                p4_s1                <= '0';
                p8_s1                <= '0';
                p16_s1               <= '0';
                p32_s1               <= '0';
                p64_s1               <= '0';
                p128_s1              <= '0';
                p256_s1              <= '0';
                p512_s1              <= '0';
                p1024_s1             <= '0';
                p2048_s1             <= '0';
                p1_s2                <= '0';
                p2_s2                <= '0';
                p4_s2                <= '0';
                p8_s2                <= '0';
                p16_s2               <= '0';
                p32_s2               <= '0';
                p64_s2               <= '0';
                p128_s2              <= '0';
                p256_s2              <= '0';
                p512_s2              <= '0';
                p1024_s2             <= '0';
                p2048_s2             <= '0';  
                p8_eq_s              <= '0';
                p16_eq_s             <= '0';
                p32_eq_s             <= '0';
                p64_eq_s             <= '0';
                p128_eq_s            <= '0';
                p256_eq_s            <= '0';
                p512_eq_s            <= '0';
                p1024_eq_s           <= '0';
                p2048_eq_s           <= '0';               
                p8_strb_s            <= '0';
                p16_strb_s           <= '0';
                p32_strb_s           <= '0';
                p64_strb_s           <= '0';
                p128_strb_s          <= '0';
                p256_strb_s          <= '0';
                p512_strb_s          <= '0';
                p1024_strb_s         <= '0';
                p2048_strb_s         <= '0';
                ecc_wr_data_s        <= '0';
                start_ecc_wr_data_s  <= '0';
                start_ecc_wr_data_s1 <= '0';
                start_ecc_wr_data_s2 <= '0';
                
                ess_wr_word1_s     <= (others => '0');
                ess_wr_word2_s     <= (others => '0');
                ess_wr_word3_s     <= (others => '0');
                ess_wr_word4_s     <= (others => '0');
                ess_wr_word5_s     <= (others => '0');
                ess_wr_word6_s     <= (others => '0');
                ess_wr_word7_s     <= (others => '0');
                ess_wr_word8_s     <= (others => '0');
                ess_wr_word9_s     <= (others => '0');
                ess_wr_word10_s    <= (others => '0');
                ess_wr_word11_s    <= (others => '0');
                ess_wr_word12_s    <= (others => '0');
                ess_wr_word13_s    <= (others => '0');
                ess_wr_word14_s    <= (others => '0');
                ess_wr_word15_s    <= (others => '0');
                ess_wr_word16_s    <= (others => '0');
                
            else
                start_ecc_wr_data_s1 <= start_ecc_wr_data_s;
                start_ecc_wr_data_s2 <= start_ecc_wr_data_s1;
                
                if iTRANSMIT_RECEIVE = '1' and data_rdy_wr_s2 = '1' then
                    start_ecc_wr_data_s <= '1';
                else
                    start_ecc_wr_data_s <= '0';
                end if;
                
                if start_ecc_wr_data_s1 = '1' and start_ecc_wr_data_s2 = '0' then
                    ecc_wr_data_s <= '1';
                end if;
                if ecc_wr_data_s = '1' then
                    if count_wr_data_ecc_wr(8 downto 0) = "000000000" then 
                        p1_s1 <= data_in_s(6) xor data_in_s(4) xor data_in_s(2) xor data_in_s(0);
                        p1_s2 <= data_in_s(7) xor data_in_s(5) xor data_in_s(3) xor data_in_s(1);
                        
                        p2_s1 <= data_in_s(5) xor data_in_s(4) xor data_in_s(1) xor data_in_s(0);
                        p2_s2 <= data_in_s(7) xor data_in_s(6) xor data_in_s(3) xor data_in_s(2);
                        
                        p4_s1 <= data_in_s(3) xor data_in_s(2) xor data_in_s(1) xor data_in_s(0);
                        p4_s2 <= data_in_s(7) xor data_in_s(6) xor data_in_s(5) xor data_in_s(4);
                        
                        if count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(1,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word1_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(2,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word2_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;                        
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(3,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word3_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(4,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word4_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(5,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word5_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(6,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word6_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(7,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word7_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(8,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word8_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(9,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word9_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(10,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word10_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(11,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word11_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(12,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word12_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(13,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word13_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(14,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word14_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(15,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word15_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        elsif count_wr_data_ecc_wr((WR_DATA_ECC_COUNT_C-1) downto 9) = conv_std_logic_vector(16,(WR_DATA_ECC_COUNT_C-9)) then
                            ess_wr_word16_s(5 downto 0) <=p4_s2 & p4_s1 & p2_s2 & p2_s1 & p1_s2 & p1_s1;
                        end if;
                    else
                        p1_s1 <= p1_s1 xor data_in_s(6) xor data_in_s(4) xor data_in_s(2) xor data_in_s(0);
                        p1_s2 <= p1_s2 xor data_in_s(7) xor data_in_s(5) xor data_in_s(3) xor data_in_s(1);
                        
                        p2_s1 <= p2_s1 xor data_in_s(5) xor data_in_s(4) xor data_in_s(1) xor data_in_s(0);
                        p2_s2 <= p2_s2 xor data_in_s(7) xor data_in_s(6) xor data_in_s(3) xor data_in_s(2);
                        
                        p4_s1 <= p4_s1 xor data_in_s(3) xor data_in_s(2) xor data_in_s(1) xor data_in_s(0);
                        p4_s2 <= p4_s2 xor data_in_s(7) xor data_in_s(6) xor data_in_s(5) xor data_in_s(4);
                    end if;
                    
                    if count_wr_data_ecc_wr(8 downto 0) = "000000000" then 
                        p8_s1 <= data_in_s(7) xor data_in_s(6) xor data_in_s(5) xor data_in_s(4) xor data_in_s(3) xor data_in_s(2) xor data_in_s(1) xor data_in_s(0);
                        p8_eq_s <= '1';
                    elsif count_wr_data_ecc_wr(8 downto 0) = "000000001" then
                        p8_s2 <= data_in_s(7) xor data_in_s(6) xor data_in_s(5) xor data_in_s(4) xor data_in_s(3) xor data_in_s(2) xor data_in_s(1) xor data_in_s(0);                  
                        p8_eq_s <= '0';
                        p8_strb_s <= '1';
                    else
                        if p8_eq_s = '0' then
                            p8_s1 <= p8_s1 xor data_in_s(7) xor data_in_s(6) xor data_in_s(5) xor data_in_s(4) xor data_in_s(3) xor data_in_s(2) xor data_in_s(1) xor data_in_s(0);
                            p8_eq_s <= '1';
                        else
                            p8_s2 <= p8_s2 xor data_in_s(7) xor data_in_s(6) xor data_in_s(5) xor data_in_s(4) xor data_in_s(3) xor data_in_s(2) xor data_in_s(1) xor data_in_s(0);                  
                            p8_eq_s <= '0';
                            p8_strb_s <= '1';
                        end if;                        
                    end if;
                    if p8_strb_s = '1' then
                        p8_strb_s <= '0';
                    end if;
                    
                    if p8_strb_s = '1' then
                        if p16_eq_s = '0' then
                            p16_s1 <= p8_s1 xor p8_s2;
                            p16_eq_s <= '1';
                        else
                            p16_s2 <= p8_s1 xor p8_s2;
                            p16_eq_s <= '0';
                            p16_strb_s <= '1';
                        end if;
                    end if;
                    if p16_strb_s = '1' then
                        p16_strb_s <= '0';
                    end if;
 
                    if p16_strb_s = '1' then
                        if p32_eq_s = '0' then
                            p32_s1 <= p16_s1 xor p16_s2;
                            p32_eq_s <= '1';
                        else
                            p32_s2 <= p16_s1 xor p16_s2;
                            p32_eq_s <= '0';
                            p32_strb_s <= '1';
                        end if;
                    end if;
                    if p32_strb_s = '1' then
                        p32_strb_s <= '0';
                    end if;
                    
                    if p32_strb_s = '1' then
                        if p64_eq_s = '0' then
                            p64_s1 <= p32_s1 xor p32_s2;
                            p64_eq_s <= '1';
                        else
                            p64_s2 <= p32_s1 xor p32_s2;
                            p64_eq_s <= '0';
                            p64_strb_s <= '1';
                        end if;
                    end if;
                    if p64_strb_s = '1' then
                        p64_strb_s <= '0';
                    end if;

                    if p64_strb_s = '1' then
                        if p128_eq_s = '0' then
                            p128_s1 <= p64_s1 xor p64_s2;
                            p128_eq_s <= '1';
                        else
                            p128_s2 <= p64_s1 xor p64_s2;
                            p128_eq_s <= '0';
                            p128_strb_s <= '1';
                        end if;
                    end if;
                    if p128_strb_s = '1' then
                        p128_strb_s <= '0';
                    end if;

                    if p128_strb_s = '1' then
                        if p256_eq_s = '0' then
                            p256_s1 <= p128_s1 xor p128_s2;
                            p256_eq_s <= '1';
                        else
                            p256_s2 <= p128_s1 xor p128_s2;
                            p256_eq_s <= '0';
                            p256_strb_s <= '1';
                        end if;
                    end if;
                    if p256_strb_s = '1' then
                        p256_strb_s <= '0';
                    end if;

                    if p256_strb_s = '1' then
                        if p512_eq_s = '0' then
                            p512_s1 <= p256_s1 xor p256_s2;
                            p512_eq_s <= '1';
                        else
                            p512_s2 <= p256_s1 xor p256_s2;
                            p512_eq_s <= '0';
                            p512_strb_s <= '1';
                        end if;
                    end if;
                    if p512_strb_s = '1' then
                        p512_strb_s <= '0';
                    end if;

                    if p512_strb_s = '1' then
                        if p1024_eq_s = '0' then
                            p1024_s1 <= p512_s1 xor p512_s2;
                            p1024_eq_s <= '1';
                        else
                            p1024_s2 <= p512_s1 xor p512_s2;
                            p1024_eq_s <= '0';
                            p1024_strb_s <= '1';
                        end if;
                    end if;
                    if p1024_strb_s = '1' then
                        p1024_strb_s <= '0';
                    end if;
                    
                    if p1024_strb_s = '1' then
                        if p2048_eq_s = '0' then
                            p2048_s1 <= p1024_s1 xor p1024_s2;
                            p2048_eq_s <= '1';
                        else
                            p2048_s2 <= p1024_s1 xor p1024_s2;
                            p2048_eq_s <= '0';
                            p2048_strb_s <= '1';
                        end if;
                    end if;
                    if p2048_strb_s = '1' then
                        p2048_strb_s <= '0';
                    end if;
                    
                    if p2048_strb_s = '1' then
                        if count_wr_data_ecc_wr = conv_std_logic_vector(512*1+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word1_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*2+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word2_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*3+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word3_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*4+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word4_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*5+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word5_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*6+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word6_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*7+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word7_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*8+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word8_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*9+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word9_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*10+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word10_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*11+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word11_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*12+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word12_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*13+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word13_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*14+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word14_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*15+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word15_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                        elsif count_wr_data_ecc_wr = conv_std_logic_vector(512*16+8,WR_DATA_ECC_COUNT_C) then
                            ess_wr_word16_s(23 downto 6) <= p2048_s2 & p2048_s1 & p1024_s2 & p1024_s1 & p512_s2 & p512_s1 & p256_s2 & p256_s1 & p128_s2 & p128_s1 & p64_s2 & p64_s1 & p32_s2 & p32_s1 & p16_s2 & p16_s1 & p8_s2 & p8_s1;
                            ecc_wr_data_s <= '0';
                        end if;
                    end if;
                    
                    count_wr_data_ecc_wr <= count_wr_data_ecc_wr + '1';
                else
                    p8_eq_s              <= '0';
                    p16_eq_s             <= '0';
                    p32_eq_s             <= '0';
                    p64_eq_s             <= '0';
                    p128_eq_s            <= '0';
                    p256_eq_s            <= '0';
                    p512_eq_s            <= '0';
                    p1024_eq_s           <= '0';
                    p2048_eq_s           <= '0';
                    p8_strb_s            <= '0';
                    p16_strb_s           <= '0';
                    p32_strb_s           <= '0';
                    p64_strb_s           <= '0';
                    p128_strb_s          <= '0';
                    p256_strb_s          <= '0';
                    p512_strb_s          <= '0';
                    p1024_strb_s         <= '0';
                    p2048_strb_s         <= '0';
                    count_wr_data_ecc_wr <= (others => '0');
                end if;               
            end if;
        end if;
    end process ecc_wr_data;
    
    count_lngt_pckg: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then           
            if (iRST = '1') then  
                start_count_lngt_pckg_s      <= '0';
                start_count_lngt_pckg_s1     <= '0';
                start_count_lngt_pckg_s2     <= '0';
                count_lngt_one_pckg_s        <= '0';
                count_lngt_one_pckg_s1       <= '0';
                count_lngt_pckg_s            <= '0';
                lngt_one_pckg_s              <= (others => '0');
                number_count_lngt_one_pckg_s <= (others => '0');
                count_lngt_pckg1_s           <= (others => '0');
                count_lngt_pckg2_s           <= (others => '0');
                count_lngt_pckg3_s           <= (others => '0');
                count_lngt_pckg4_s           <= (others => '0');
                count_lngt_pckg5_s           <= (others => '0');
                count_lngt_pckg6_s           <= (others => '0');
                count_lngt_pckg7_s           <= (others => '0');
                count_lngt_pckg8_s           <= (others => '0');
                count_lngt_pckg9_s           <= (others => '0');
                count_lngt_pckg10_s          <= (others => '0');
                count_lngt_pckg11_s          <= (others => '0');
                count_lngt_pckg12_s          <= (others => '0');
                count_lngt_pckg13_s          <= (others => '0');
                count_lngt_pckg14_s          <= (others => '0');
                count_lngt_pckg15_s          <= (others => '0');
                count_lngt_pckg16_s          <= (others => '0');
            else
                start_count_lngt_pckg_s1 <= start_count_lngt_pckg_s;
                start_count_lngt_pckg_s2 <= start_count_lngt_pckg_s1;
                
                count_lngt_one_pckg_s1 <= count_lngt_one_pckg_s;
                
                if iTRANSMIT_RECEIVE = '1' and data_rdy_wr_s2 = '1' then
                    start_count_lngt_pckg_s <= '1';
                else
                    start_count_lngt_pckg_s <= '0';
                end if;            
                
                if start_count_lngt_pckg_s1 = '1' and start_count_lngt_pckg_s2 = '0' then
                    count_lngt_pckg_s <= '1';
                elsif start_count_lngt_pckg_s1 = '0' and start_count_lngt_pckg_s2 = '1' then
                    count_lngt_pckg_s <= '0';
                end if;
                
                if count_lngt_pckg_s = '1' then
                    if data_in_s = x"7E" and count_lngt_one_pckg_s = '0' then
                        count_lngt_one_pckg_s <= '1';
                        if number_count_lngt_one_pckg_s = conv_std_logic_vector(0,4) then
                            count_lngt_pckg1_s <= conv_std_logic_vector(1,16); 
                            count_lngt_pckg2_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg3_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg4_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg5_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg6_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg7_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg8_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg9_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg10_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg11_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg12_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg13_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg14_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg15_s <= conv_std_logic_vector(0,16); 
                            count_lngt_pckg16_s <= conv_std_logic_vector(0,16); 
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(1,4) then
                            count_lngt_pckg2_s <= count_lngt_pckg2_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(2,4) then
                            count_lngt_pckg3_s <= count_lngt_pckg3_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(3,4) then
                            count_lngt_pckg4_s <= count_lngt_pckg4_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(4,4) then
                            count_lngt_pckg5_s <= count_lngt_pckg5_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(5,4) then
                            count_lngt_pckg6_s <= count_lngt_pckg6_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(6,4) then
                            count_lngt_pckg7_s <= count_lngt_pckg7_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(7,4) then
                            count_lngt_pckg8_s <= count_lngt_pckg8_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(8,4) then
                            count_lngt_pckg9_s <= count_lngt_pckg9_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(9,4) then
                            count_lngt_pckg10_s <= count_lngt_pckg10_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(10,4) then
                            count_lngt_pckg11_s <= count_lngt_pckg11_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(11,4) then
                            count_lngt_pckg12_s <= count_lngt_pckg12_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(12,4) then
                            count_lngt_pckg13_s <= count_lngt_pckg13_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(13,4) then
                            count_lngt_pckg14_s <= count_lngt_pckg14_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(14,4) then
                            count_lngt_pckg15_s <= count_lngt_pckg15_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(15,4) then
                            count_lngt_pckg16_s <= count_lngt_pckg16_s + '1';
                        end if;   
                    elsif data_in_s = x"7E" and count_lngt_one_pckg_s = '1' then
                        count_lngt_one_pckg_s <= '0';
                        if number_count_lngt_one_pckg_s < conv_std_logic_vector(15,4) then
                            number_count_lngt_one_pckg_s <= number_count_lngt_one_pckg_s + '1';
                        end if;
                    end if;
                    
                    if count_lngt_one_pckg_s = '1' then
                        if number_count_lngt_one_pckg_s = conv_std_logic_vector(0,4) then
                            count_lngt_pckg1_s <= count_lngt_pckg1_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(1,4) then
                            count_lngt_pckg2_s <= count_lngt_pckg2_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(2,4) then
                            count_lngt_pckg3_s <= count_lngt_pckg3_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(3,4) then
                            count_lngt_pckg4_s <= count_lngt_pckg4_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(4,4) then
                            count_lngt_pckg5_s <= count_lngt_pckg5_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(5,4) then
                            count_lngt_pckg6_s <= count_lngt_pckg6_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(6,4) then
                            count_lngt_pckg7_s <= count_lngt_pckg7_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(7,4) then
                            count_lngt_pckg8_s <= count_lngt_pckg8_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(8,4) then
                            count_lngt_pckg9_s <= count_lngt_pckg9_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(9,4) then
                            count_lngt_pckg10_s <= count_lngt_pckg10_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(10,4) then
                            count_lngt_pckg11_s <= count_lngt_pckg11_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(11,4) then
                            count_lngt_pckg12_s <= count_lngt_pckg12_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(12,4) then
                            count_lngt_pckg13_s <= count_lngt_pckg13_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(13,4) then
                            count_lngt_pckg14_s <= count_lngt_pckg14_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(14,4) then
                            count_lngt_pckg15_s <= count_lngt_pckg15_s + '1';
                        elsif number_count_lngt_one_pckg_s = conv_std_logic_vector(15,4) then
                            count_lngt_pckg16_s <= count_lngt_pckg16_s + '1';
                        end if; 
                    end if;
                else
                    number_count_lngt_one_pckg_s <= (others => '0');
                end if;
            end if;
        end if;
    end process count_lngt_pckg;
    
    read_data: process(iCLK_100MHZ)
    begin
        if(iCLK_100MHZ'event and iCLK_100MHZ = '1') then           
            if (iRST = '1') then
                rd_data_count_s <= conv_std_logic_vector(0,RD_DATA_COUNT_C);
                addr_strt_rd_dt_s3    <= (others => '0');
                dq_in_rd_dt_s    <= (others => '0');
                strt_we_rd_data_count_s <= '0';
                strt_re_rd_data_count_s <= '0';
                
                cle_rd_dt_s          <= '0';            
                ce_rd_dt_s           <= (others => '1');              
                ale_rd_dt_s          <= '0';         
       
                wp_rd_dt_s           <= '1';       
                dq_out_rd_dt_s       <= (others => '0');                         
                dq_t_rd_dt_s         <= '0';  
                
                data_rdy_rd_s    <= '0';   
            else
                if start_rd_data_s2 = '1' then
                    if rd_data_count_s = conv_std_logic_vector(0,RD_DATA_COUNT_C) then
                        rd_data_count_s <= conv_std_logic_vector(1,RD_DATA_COUNT_C);
                        addr_strt_rd_dt_s3 <= addr_strt_rd_dt_s2;
                    elsif rd_data_count_s < conv_std_logic_vector(4,RD_DATA_COUNT_C) then
                        if addr_strt_rd_dt_s3(35 downto 33) = "000" then
                            ce_rd_dt_s <= "11111110";
                        elsif addr_strt_rd_dt_s3(35 downto 33) = "001" then
                            ce_rd_dt_s <= "11111101";                   
                        elsif addr_strt_rd_dt_s3(35 downto 33) = "010" then
                            ce_rd_dt_s <= "11111011";                    
                        elsif addr_strt_rd_dt_s3(35 downto 33) = "011" then
                            ce_rd_dt_s <= "11110111";
                        elsif addr_strt_rd_dt_s3(35 downto 33) = "100" then
                            ce_rd_dt_s <= "11101111";
                        elsif addr_strt_rd_dt_s3(35 downto 33) = "101" then
                            ce_rd_dt_s <= "11011111";
                        elsif addr_strt_rd_dt_s3(35 downto 33) = "110" then
                            ce_rd_dt_s <= "10111111";
                        elsif addr_strt_rd_dt_s3(35 downto 33) = "111" then
                            ce_rd_dt_s <= "01111111";
                        end if;
                        strt_we_rd_data_count_s <= '1';
                        cle_rd_dt_s <= '1';
                        ale_rd_dt_s <= '0';
                        dq_out_rd_dt_s <= X"00";
                        dq_t_rd_dt_s <= '0';
                        rd_data_count_s <= rd_data_count_s + '1';
                    elsif rd_data_count_s < conv_std_logic_vector(6,RD_DATA_COUNT_C) then
                        cle_rd_dt_s <= '0';
                        ale_rd_dt_s <= '1';
                        dq_out_rd_dt_s <= addr_strt_rd_dt_s3(7 downto 0);
                        dq_t_rd_dt_s <= '0';
                        rd_data_count_s <= rd_data_count_s + '1';
                    elsif rd_data_count_s < conv_std_logic_vector(10-2,RD_DATA_COUNT_C) then
                        dq_out_rd_dt_s <= "000" & addr_strt_rd_dt_s3(12 downto 8);
                        dq_t_rd_dt_s <= '0';
                        rd_data_count_s <= rd_data_count_s + '1';
                    elsif rd_data_count_s < conv_std_logic_vector(12-2,RD_DATA_COUNT_C) then
                        dq_out_rd_dt_s <= addr_strt_rd_dt_s3(20 downto 13);
                        dq_t_rd_dt_s <= '0';
                        rd_data_count_s <= rd_data_count_s + '1';
                    elsif rd_data_count_s < conv_std_logic_vector(14-2,RD_DATA_COUNT_C) then
                        dq_out_rd_dt_s <= addr_strt_rd_dt_s3(28 downto 21);
                        dq_t_rd_dt_s <= '0';
                        rd_data_count_s <= rd_data_count_s + '1';
                    elsif rd_data_count_s < conv_std_logic_vector(16-2,RD_DATA_COUNT_C) then
                        dq_out_rd_dt_s <= "0000" & addr_strt_rd_dt_s3(32 downto 29);
                        dq_t_rd_dt_s <= '0';
                        rd_data_count_s <= rd_data_count_s + '1';
                    elsif rd_data_count_s = conv_std_logic_vector(16-2,RD_DATA_COUNT_C) then
                        strt_we_rd_data_count_s <= '0';
                        rd_data_count_s <= rd_data_count_s + '1';
                        ale_rd_dt_s <= '0';
                        cle_rd_dt_s <= '1';
                        dq_out_rd_dt_s <= X"30";
                        dq_t_rd_dt_s <= '0';                        
                    elsif rd_data_count_s < conv_std_logic_vector(18-2,RD_DATA_COUNT_C) then
                        rd_data_count_s <= rd_data_count_s + '1';
                    elsif rd_data_count_s < conv_std_logic_vector(18,RD_DATA_COUNT_C) then
                        cle_rd_dt_s <= '0';
                        dq_out_rd_dt_s <= (others => '0');
                        dq_t_rd_dt_s <= '0';
                        rd_data_count_s <= rd_data_count_s + '1';
                    elsif rd_data_count_s = conv_std_logic_vector(18,RD_DATA_COUNT_C) then
                        if rb_s3 = '0' then
                            rd_data_count_s <= rd_data_count_s + '1';
                        end if;
                    elsif rd_data_count_s = conv_std_logic_vector(19,RD_DATA_COUNT_C) then
                        if rb_s3 = 'H' then
                            rd_data_count_s <= rd_data_count_s + '1';
                            dq_t_rd_dt_s <= '1';
                        end if;                    
                    elsif rd_data_count_s < conv_std_logic_vector(24,RD_DATA_COUNT_C) then 
                        if rd_data_count_s < conv_std_logic_vector(23,RD_DATA_COUNT_C) then
                            rd_data_count_s <= rd_data_count_s + '1';
                        elsif rd_data_count_s = conv_std_logic_vector(23,RD_DATA_COUNT_C) then
                            data_rdy_rd_s <= '1';
                        end if;
                        strt_re_rd_data_count_s <= '1';
                        if re_rd_data_count_s = "01" then
                            dq_in_rd_dt_s <= dq_in_s;
                        end if;                   
                    end if;
                else
                    cle_rd_dt_s             <= '0';            
                    ce_rd_dt_s              <= (others => '1');              
                    ale_rd_dt_s             <= '0';                
                    wp_rd_dt_s              <= '1';       
        --            dq_out_wr_dt_s          <= (others => '0');              
                    dq_t_rd_dt_s            <= '0'; 
                    data_rdy_rd_s        <= '0';
                    rd_data_count_s         <= (others => '0');
                    dq_in_rd_dt_s         <= (others => '0');
                    strt_re_rd_data_count_s <= '0';
                    strt_we_rd_data_count_s <= '0';                    
                end if;
            end if;
        end if;
    end process read_data;
    
           
oLD               <= ld_s or ld_tr_cmd_mode1_s or ld_st_ftrs_md1_s or ld_strt_wr_dt_s or ld_strt_rd_dt_s;
oCLE              <= cle_s or cle_s1 or cle_tr_cmd_mode1_s or cle_st_ftrs_md1_s or cle_wr_dt_s or cle_rd_dt_s;
oCE               <= ce_s and ce_s1 and ce_tr_cmd_mode1_s and ce_st_ftrs_md1_s and ce_wr_dt_s and ce_rd_dt_s;
oWE               <= we_s and we_s1 and we_tr_cmd_mode1_s and we_st_ftrs_md1_s and we_wr_dt_s;
oALE              <= ale_s or ale_s1 or ale_tr_cmd_mode1_s or ale_st_ftrs_md1_s or ale_wr_dt_s or ale_rd_dt_s; 
oRE               <= re_s and re_tr_cmd_mode1_s and re_st_ftrs_md1_s and re_wr_dt_s and re_rd_dt_s;
oWP               <= wp_s and wp_tr_cmd_mode1_s and wp_st_ftrs_md1_s and wp_wr_dt_s and wp_rd_dt_s;
oDQ               <= dq_out_s or dq_out_s1 or dq_out_tr_cmd_mode1_s or dq_out_st_ftrs_md1_s or dq_out_wr_dt_s or dq_out_rd_dt_s; 
oDQ_T             <= dq_t_s or dq_t_s1 or dq_t_tr_cmd_mode1_s or dq_t_st_ftrs_md1_s or dq_t_wr_dt_s or dq_t_rd_dt_s;
oRB               <= rb_s1;
oTRANSMIT_COMMAND <= transmit_command_mode1_s;
oSET_FEATURES     <= set_features_mode1_s;
dq_in_s           <= iDQ;
data_in_s         <= iDATA;
oDATA_RDY_WR_RD   <= data_rdy_wr_s2 or data_rdy_rd_s2;
oDATA             <= dq_in_rd_dt_s2;

end arch;
