----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:48:22 05/29/2013 
-- Design Name: 
-- Module Name:    nand_driver_io - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity nand_driver_io is
	generic( 
            WIDTH_ADDR_NAND_DR  : integer := 36; --width addr bus
            WIDTH_DATA_NAND_DR  : integer := 8;   --width data bus
            WIDTH_COMMAND       : integer := 8;   --width command bus
            WIDTH_CE            : integer := 8   --width CE bus
            );
    port(
        --IN--
        iCLK_50MHZ          : in std_logic;                                                        -- input clock, 50 MHz.
        iCLK_100MHZ          : in std_logic;                                                        -- input clock, 100 MHz.
        iRST                : in std_logic;                                                        -- reset
        iCOMMAND_DATA       : in std_logic;                                                        -- iCOMMAND_DATA = 0 -transmit/receive command; iCOMMAND_DATA = 1 -transmit/receive data
        iSET_FEATURES       : in std_logic;                                                        -- iSET_FEATURES = 1 - set features
        iCOMMAND            : in std_logic_vector((WIDTH_COMMAND-1) downto 0);        
        iRD                 : in std_logic;                                                        -- iRD = 1 - reade; 
        iWR                 : in std_logic;                                                        -- iWR = 1 - write;
        iTRANSMIT_RECEIVE   : in std_logic;                                                        -- iWR = 1 - write;
        iADDR               : in std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0);
        iDATA               : in std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);
        iRB                 : in std_logic; 
        --OUT--
        oDATA               : out std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);
        oDATA_RDY_WR_RD     : out std_logic;                                                       -- oDATA_RDY_WR_RD = 1 - data ready for read/write;
        oLD                 : out std_logic;                                                       -- oLD = 1 - load command
        oTRANSMIT_COMMAND   : out std_logic;                                                       -- oTRANSMIT_COMMAND = 1 -transmit command
        oSET_FEATURES       : out std_logic;                                                       -- oSET_FEATURES = 1 - set features
        oCLE                : out std_logic;                                                       -- NAND signal
        oCE                 : out std_logic_vector((WIDTH_CE-1) downto 0);                         -- NAND signal
        oWE                 : out std_logic;                                                       -- NAND signal
        oALE                : out std_logic;                                                       -- NAND signal
        oRE                 : out std_logic;                                                       -- NAND signal
        oWP                 : out std_logic;                                                       -- NAND signal
        oRB                 : out std_logic;                                                       -- NAND signal
                                                                                                   --INOUT
        ioDQ                : inout std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0)              -- NAND signal
		);
end nand_driver_io;

architecture Behavioral of nand_driver_io is

	COMPONENT nand_driver
    generic ( 
            WIDTH_ADDR_NAND_DR  : integer := WIDTH_ADDR_NAND_DR; --width addr bus
            WIDTH_DATA_NAND_DR  : integer := WIDTH_DATA_NAND_DR;   --width data bus
            WIDTH_COMMAND       : integer := WIDTH_COMMAND     ;   --width command bus
            WIDTH_CE            : integer := WIDTH_CE             --width CE bus
            );
	port(
        --IN--
        iCLK_50MHZ                : in std_logic;                                                        -- input clock, 40 MHz.
        iCLK_100MHZ                : in std_logic;                                                        -- input clock, 40 MHz.
        iRST                : in std_logic;                                                        -- reset
        iCOMMAND_DATA       : in std_logic;                                                        -- iCOMMAND_DATA = 0 -transmit/receive command; iCOMMAND_DATA = 1 -transmit/receive data
        iSET_FEATURES       : in std_logic;                                                        -- iSET_FEATURES = 1 - set features
        iCOMMAND            : in std_logic_vector((WIDTH_COMMAND-1) downto 0);       
        iRD                 : in std_logic;                                                        -- iRD = 1 - reade; 
        iWR                 : in std_logic;                                                        -- iWR = 1 - write;
        iTRANSMIT_RECEIVE   : in std_logic;                                                        -- iWR = 1 - write;
        iADDR               : in std_logic_vector((WIDTH_ADDR_NAND_DR-1) downto 0);
        iDATA               : in std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);
        iDQ                 : in std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);
        iRB                 : in std_logic; 
        --OUT--
        oDATA               : out std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);
        oDATA_RDY_WR_RD     : out std_logic;                                                       -- oDATA_RDY_WR_RD = 1 - data ready for read/write;
        oLD                 : out std_logic;                                                       -- oLD = 1 - load command
        oTRANSMIT_COMMAND   : out std_logic;                                                       -- oTRANSMIT_COMMAND = 1 -transmit command
        oSET_FEATURES       : out std_logic;                                                       -- oSET_FEATURES = 1 - set features
        oCLE                : out std_logic;                                                       -- NAND signal
        oCE                 : out std_logic_vector((WIDTH_CE-1) downto 0);                         -- NAND signal
        oWE                 : out std_logic;                                                       -- NAND signal
        oALE                : out std_logic;                                                       -- NAND signal
        oRE                 : out std_logic;                                                       -- NAND signal
        oWP                 : out std_logic;                                                       -- NAND signal
        oRB                 : out std_logic;                                                       -- NAND signal
        oDQ                 : out std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0);               -- NAND signal
        oDQ_T               : out std_logic                                                        -- oDQ_T = 0 - oDQ-output; oDQ_T = 1 - oDQ-input  
		);
	END COMPONENT;

    signal  dq_in_s            : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
    signal  dq_out_s           : std_logic_vector((WIDTH_DATA_NAND_DR-1) downto 0) := (others => '0');
    signal  dq_t_s             : std_logic := '0';
    
begin

	Inst_nand_driver: nand_driver PORT MAP(
		iCLK_50MHZ              => iCLK_50MHZ             ,
		iCLK_100MHZ              => iCLK_100MHZ             ,
		iRST              => iRST             ,
		iCOMMAND_DATA     => iCOMMAND_DATA    ,
		iSET_FEATURES     => iSET_FEATURES    ,
		iCOMMAND          => iCOMMAND         ,
		iRD               => iRD              ,
		iWR               => iWR              ,
		iTRANSMIT_RECEIVE => iTRANSMIT_RECEIVE,
		iRB               => iRB              ,
		iADDR             => iADDR            ,
		iDATA             => iDATA            ,
		iDQ               => dq_in_s          ,
		oDATA             => oDATA            ,
		oDATA_RDY_WR_RD   => oDATA_RDY_WR_RD  ,
		oLD               => oLD              ,
		oTRANSMIT_COMMAND => oTRANSMIT_COMMAND,
		oSET_FEATURES     => oSET_FEATURES    ,
		oCLE              => oCLE             ,
		oCE               => oCE              ,
		oWE               => oWE              ,
		oALE              => oALE             ,
		oRE               => oRE              ,
		oWP               => oWP              ,
		oRB               => oRB              ,
		oDQ               => dq_out_s         ,
		oDQ_T             => dq_t_s            
	);
    
    IOBUF_inst_for : for i in (WIDTH_DATA_NAND_DR-1) downto 0 generate
        IOBUF_inst : IOBUF
        generic map (
            DRIVE => 12,
            IOSTANDARD => "DEFAULT",
            SLEW => "SLOW")
        port map (
            O   => dq_in_s(i),       -- Buffer output
            IO  => ioDQ(i),         -- Buffer inout port (connect directly to top-level port)
            I   => dq_out_s(i),      -- Buffer input
            T   => dq_t_s         -- 3-state enable input, high=input, low=output 
        );
    end generate;

   
   --IBUF_MUX_LABLE : for i in 2 downto 0 generate
--IBUF_MUX : IBUF
--   generic map (
--      IBUF_DELAY_VALUE => "0", -- Specify the amount of added input delay for buffer, "0"-"16" (Spartan-3E/3A only)
--      IFD_DELAY_VALUE => "AUTO", -- Specify the amount of added delay for input register, "AUTO", "0"-"8" (Spartan-3E/3A only)
--      IOSTANDARD => "DEFAULT")
--   port map (
--      O => MUXi(i),     -- Buffer output
--      I => MUX(i)      -- Buffer input (connect directly to top-level port)
--   );
--end generate;

end Behavioral;

