----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:30:24 06/24/2013 
-- Design Name: 
-- Module Name:    fifo1_2048_top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
    use IEEE.STD_LOGIC_ARITH.ALL;  

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fifo1_2048_top is
    generic( 
            WIDTH_DATA_FIFO1  : integer := 8;   --width data bus
            WIDTH_DATA_COUNT  : integer := 11   --width data bus
            );
    port(
        --IN--
        iCLK_50MHZ          : in std_logic;                                         -- input clock, 50 MHz.
        iRST                : in std_logic;                                         -- reset
        iDATA_DEV           : in std_logic_vector((WIDTH_DATA_FIFO1-1) downto 0);   -- interface input data
        iSTART_PACK_DEV     : in std_logic;                                         -- interface pulse signal. packet is trnsmite
        iWRDREQ_DEV         : in std_logic;
        iRD_DATA_FAFO1      : in std_logic;
        --OUT--
        oEMPTY_FIFO1        : out std_logic;
        oFULL_FIFO1         : out std_logic;
        oVALID_FIFO1        : out std_logic;
        oDATA_FIFO1         : out std_logic_vector((WIDTH_DATA_FIFO1-1) downto 0); 
        oCOUNT_PACK_FIFO1   : out std_logic_vector((WIDTH_DATA_COUNT-1) downto 0);
        oPACK_RCV_FIFO1     : out std_logic        
        );
end fifo1_2048_top;

architecture Behavioral of fifo1_2048_top is

    signal emty_fifo1_s     : std_logic;
    signal valid_fifo1_s     : std_logic;
    signal full_fifo1_s     : std_logic;
    signal wr_en_fifo1_s    : std_logic :='0';
    signal wr_en_fifo1_s1    : std_logic :='0';
    signal wr_en_fifo1_s2    : std_logic :='0';
    signal wr_en_fifo1_s3    : std_logic :='0';
    signal start_pack_dev_s   : std_logic :='0';
    signal start_pack_dev_s1    : std_logic :='0';
    signal rd_en_fifo1_s    : std_logic :='0';
    signal wrdreq_dev_fifo1_s    : std_logic;
    signal data_out_s       : std_logic_vector((WIDTH_DATA_FIFO1-1) downto 0);
    signal data_in_s        : std_logic_vector((WIDTH_DATA_FIFO1-1) downto 0);
    signal data_count_s     : std_logic_vector((WIDTH_DATA_COUNT-1) downto 0);
    signal count_pack_s     : std_logic_vector((WIDTH_DATA_COUNT-1) downto 0);
	
    COMPONENT fifo_generator_v8_2
	PORT(
	    clk        : IN std_logic;
		srst       : IN std_logic;
		din        : IN std_logic_vector((WIDTH_DATA_FIFO1-1) downto 0);
		wr_en      : IN std_logic;
		rd_en      : IN std_logic;          
		dout       : OUT std_logic_vector((WIDTH_DATA_FIFO1-1) downto 0);
		full       : OUT std_logic;
		empty      : OUT std_logic;
        valid      : OUT std_logic;
		data_count : OUT std_logic_vector((WIDTH_DATA_COUNT-1) downto 0)
		);
	END COMPONENT;


begin

	Inst_fifo_generator_v8_2: fifo_generator_v8_2 PORT MAP(
		clk        => iCLK_50MHZ,
		srst       => iRST,
		din        => data_in_s,
		wr_en      => wr_en_fifo1_s,
		rd_en      => rd_en_fifo1_s,
		dout       => data_out_s,
		full       => full_fifo1_s,
		empty      => emty_fifo1_s,
        valid      => valid_fifo1_s,
		data_count => data_count_s
	);
    
    write_to_fifo1: process(iCLK_50MHZ)
    begin
        if(iCLK_50MHZ'event and iCLK_50MHZ = '1') then           
            if (iRST = '1') then  
                data_in_s <= (others => '0');
                wr_en_fifo1_s1 <= '0';
                wr_en_fifo1_s2 <= '0';
                wr_en_fifo1_s3 <= '0';
                start_pack_dev_s <= '0';
                start_pack_dev_s1 <= '0';
                count_pack_s <= (others => '0');
                
            else
                wr_en_fifo1_s2 <= wr_en_fifo1_s1;
                wr_en_fifo1_s3 <= wr_en_fifo1_s2;
                
                data_in_s <= iDATA_DEV;
                
                start_pack_dev_s <= iSTART_PACK_DEV;
                start_pack_dev_s1 <= start_pack_dev_s;
                
                if iDATA_DEV = x"7E" and wr_en_fifo1_s1 = '0' then
                    wr_en_fifo1_s1 <= '1';
--                elsif data_in_s = x"7E" and wr_en_fifo1_s = '1' and data_count_s > conv_std_logic_vector(0,WIDTH_DATA_COUNT) then
                elsif iDATA_DEV = x"7E" and wr_en_fifo1_s1 = '1' then
                    wr_en_fifo1_s1 <= '0';
                end if;
                
                -- if wr_en_fifo1_s1 = '0' and wr_en_fifo1_s2 = '1' then
                    -- count_pack_s <= data_count_s;                 
                -- end if;
                if wr_en_fifo1_s2 = '0' and wr_en_fifo1_s3 = '1' then
                    count_pack_s <= data_count_s;                 
                end if;
                
            end if;
        end if;
    end process write_to_fifo1;

wr_en_fifo1_s <= wr_en_fifo1_s1 or wr_en_fifo1_s2;
    
oEMPTY_FIFO1        <= emty_fifo1_s;
oFULL_FIFO1         <= full_fifo1_s; 
oDATA_FIFO1         <= data_out_s;  
oCOUNT_PACK_FIFO1   <= count_pack_s;
oPACK_RCV_FIFO1     <= start_pack_dev_s1;
oVALID_FIFO1        <= valid_fifo1_s;
rd_en_fifo1_s       <= iRD_DATA_FAFO1;
wrdreq_dev_fifo1_s  <= iWRDREQ_DEV;

end Behavioral;

