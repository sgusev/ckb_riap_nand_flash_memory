--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:16:54 05/29/2013
-- Design Name:   
-- Module Name:   E:/work/CKB_RIAP/nand_flash_memory/fpga_project/xilinx/test_nand_driver_io.vhd
-- Project Name:  nand
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: nand_driver_io
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_nand_driver_io IS

    -- PORT(
         -- iCLK              : IN  std_logic;
         -- iRST              : IN  std_logic;
         -- iCOMMAND_DATA     : IN  std_logic;
         -- iSET_FEATURES     : IN  std_logic;
         -- iCOMMAND          : IN  std_logic_vector(7 downto 0);
         -- iRD               : IN  std_logic;
         -- iWR               : IN  std_logic;
         -- iADDR             : IN  std_logic_vector(15 downto 0);
         -- iDATA             : IN  std_logic_vector(7 downto 0);
         -- oDATA             : OUT  std_logic_vector(7 downto 0);
         -- oDATA_RDY_WR_RD   : OUT  std_logic;
         -- oLD               : OUT  std_logic;
         -- oTRANSMIT_COMMAND : OUT  std_logic;
         -- oSET_FEATURES     : OUT  std_logic;
-- --         oCLE              : OUT  std_logic;
-- --         oCE               : OUT  std_logic_vector(7 downto 0);
-- --         oWE               : OUT  std_logic;
-- --         oALE              : OUT  std_logic;
-- --         oRE               : OUT  std_logic;
-- --         oWP               : OUT  std_logic;
         -- oRB               : OUT  std_logic
        -- );

END test_nand_driver_io;
 
ARCHITECTURE behavior OF test_nand_driver_io IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT nand_driver_io
    PORT(
         iCLK              : IN  std_logic;
         iRST              : IN  std_logic;
         iCOMMAND_DATA     : IN  std_logic;
         iSET_FEATURES     : IN  std_logic;
         iCOMMAND          : IN  std_logic_vector(7 downto 0);
         iRD               : IN  std_logic;
         iWR               : IN  std_logic;
         iADDR             : IN  std_logic_vector(15 downto 0);
         iDATA             : IN  std_logic_vector(7 downto 0);
         oDATA             : OUT  std_logic_vector(7 downto 0);
         oDATA_RDY_WR_RD   : OUT  std_logic;
         oLD               : OUT  std_logic;
         oTRANSMIT_COMMAND : OUT  std_logic;
         oSET_FEATURES     : OUT  std_logic;
         oCLE              : OUT  std_logic;
         oCE               : OUT  std_logic_vector(7 downto 0);
         oWE               : OUT  std_logic;
         oALE              : OUT  std_logic;
         oRE               : OUT  std_logic;
         oWP               : OUT  std_logic;
         ioDQ              : INOUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    
    COMPONENT nand_model
	PORT(
		Cle      : IN std_logic;
		Ale      : IN std_logic;
		Clk_We_n : IN std_logic;
		Wr_Re_n  : IN std_logic;
		Ce_n     : IN std_logic; --IN std_logic
		Ce2_n    : IN std_logic; --IN std_logic
		Wp_n     : IN std_logic;    
		Dq_Io    : INOUT std_logic_vector(7 downto 0); --0 to 0
		Dqs      : INOUT std_logic;      
		Rb_n     : OUT std_logic
		);
	END COMPONENT;
    

   --Inputs
   signal iCLK : std_logic := '0';
   signal iRST : std_logic := '0';
   signal iCOMMAND_DATA : std_logic := '0';
   signal iSET_FEATURES : std_logic := '0';
   signal iCOMMAND : std_logic_vector(7 downto 0) := (others => '0');
   signal iRD : std_logic := '0';
   signal iWR : std_logic := '0';
   signal iADDR : std_logic_vector(15 downto 0) := (others => '0');
   signal iDATA : std_logic_vector(7 downto 0) := (others => '0');

	--BiDirs
   signal ioDQ_s : std_logic_vector(7 downto 0);
   signal ioDQs_s : std_logic;

 	--Outputs
   signal oDATA : std_logic_vector(7 downto 0);
   signal oDATA_RDY_WR_RD : std_logic;
   signal oLD : std_logic;
   signal oTRANSMIT_COMMAND : std_logic;
   signal oSET_FEATURES : std_logic;
   signal oCLE_s : std_logic;
   signal oCE : std_logic_vector(7 downto 0);
   signal oCE_1 : std_logic_vector(1 downto 0);
   signal oWE : std_logic;
   signal oALE : std_logic;
   signal oRE : std_logic;
   signal oWP : std_logic;
   signal oRB : std_logic;

   -- Clock period definitions
   constant iCLK_period : time := 25 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: nand_driver_io PORT MAP (
          iCLK              => iCLK,
          iRST              => iRST,
          iCOMMAND_DATA     => iCOMMAND_DATA,
          iSET_FEATURES     => iSET_FEATURES,
          iCOMMAND          => iCOMMAND,
          iRD               => iRD,
          iWR               => iWR,
          iADDR             => iADDR,
          iDATA             => iDATA,
          oDATA             => oDATA,
          oDATA_RDY_WR_RD   => oDATA_RDY_WR_RD,
          oLD               => oLD,
          oTRANSMIT_COMMAND => oTRANSMIT_COMMAND,
          oSET_FEATURES     => oSET_FEATURES,
          oCLE              => oCLE_s,
          oCE               => oCE,
          oWE               => oWE,
          oALE              => oALE,
          oRE               => oRE,
          oWP               => oWP,
          ioDQ              => ioDQ_s
        );
        
    Inst_nand_model: nand_model PORT MAP(
		Dq_Io    => ioDQ_s(7 downto 0),
		Cle      => oCLE_s,
		Ale      => oALE,
		Clk_We_n => oWE,
		Wr_Re_n  => oRE,
		Dqs      => ioDQs_s,
		Ce_n     => oCE(0),
		Ce2_n    => oCE(1),
		Wp_n     => oWP,
		Rb_n     => oRB
	);

   -- Clock process definitions
   iCLK_process :process
   begin
		iCLK <= '0';
		wait for iCLK_period/2;
		iCLK <= '1';
		wait for iCLK_period/2;
   end process;
 

   oCE_1(1 downto 0) <= oCE(1 downto 0);
   -- Stimulus process
   stim_proc: process
   begin		     
        iRST          <= '1';   
        iCOMMAND_DATA <= '0';
        iSET_FEATURES <= '0';
        iCOMMAND      <= (others => '0');
        iRD           <= '0';
        iWR           <= '0';
        iADDR         <= (others => '0');
        iDATA         <= (others => '0');
        
        wait for 100 ns;	

        wait until iCLK = '1';
        iRST          <= '0'; 

      -- insert stimulus here 

      wait;
   end process;

END;
